package com.latam.ancoff;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.Status;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.latam.ancoff.controller.HealthCheckController;
import com.latam.ancoff.utils.AppProperties;

import lombok.extern.slf4j.Slf4j;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class HealthCheckControllerTest {

    HealthCheckController controller = new HealthCheckController();

    AppProperties appProperties = new AppProperties();
    
    private static String prueba = "metodo de prueba";
    
    Status statusUp = Status.UP;
    Status statusDown = Status.DOWN;
    Boolean result = false;

    Health.Builder builder = new Health.Builder(statusUp);
    
    
    @Before
    public void fillData() {
        appProperties.setSessionCreateEndpoint("http://ldsvsoaap04.cl.lan.com:12102/WSSessionManager-4.0-webservice/SessionCreate");
        appProperties.setSessionCloseEndpoint("http://ldsvsoaap04.cl.lan.com:12102/WSSessionManager-4.0-webservice/SessionClose");
        
    }
    
    @Test
    public void testHealthCheck() {

        appProperties.setSessionCreateEndpoint(
                "http://ldsvsoaap04.cl.lan.com:12102/WSSessionManager-4.0-webservice/SessionCreate");
        appProperties.setSessionCloseEndpoint(
                "http://ldsvsoaap04.cl.lan.com:12102/WSSessionManager-4.0-webservice/SessionClose");
        
        assertTrue(prueba, testGenericHealthCheck(statusUp));

        appProperties.setSessionCreateEndpoint("https://");
        appProperties.setSessionCloseEndpoint("https://");
        testGenericHealthCheck(statusDown);
        assertFalse(prueba, testGenericHealthCheck(statusDown));
        
        appProperties.setSessionCreateEndpoint("");
        appProperties.setSessionCloseEndpoint("");
        testGenericHealthCheck(statusDown);
        assertTrue(prueba, testGenericHealthCheck(statusDown));

    }
    
    
    private boolean testGenericHealthCheck(Status status)  {
        
        builder = new Health.Builder(status);
        
        try {
            controller.setAppProperties(appProperties);
            controller.doHealthCheck(builder);
            result = true;
            assertTrue(prueba, result);
            
        } catch (Exception e) {
            log.info("Error testGenericHealthCheck : ", e);
            result = false;
            assertFalse(prueba, result);
        }  
        return result;
    }
   
}
