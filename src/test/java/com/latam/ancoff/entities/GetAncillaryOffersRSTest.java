package com.latam.ancoff.entities;


import java.math.BigDecimal;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import com.latam.ancoff.entities.utils.ConstantsTest;
import com.latam.pax.ancillary.offer.Ancillary;
import com.latam.pax.ancillary.offer.AncillaryFee;
import com.latam.pax.ancillary.offer.BaggageAllowanceOC;
import com.latam.pax.ancillary.offer.ElectronicMiscDocType;
import com.latam.pax.ancillary.offer.EnumAncillaryReasonForIssuance;
import com.latam.pax.ancillary.offer.EnumElectronicMiscDocType;
import com.latam.pax.ancillary.offer.EnumServiceTypes;
import com.latam.pax.ancillary.offer.GetAncillaryOffersRS;
import com.latam.pax.ancillary.offer.Money;
import com.latam.pax.ancillary.offer.Offers;
import com.latam.pax.ancillary.offer.PassengerOffers;
import com.latam.pax.ancillary.offer.PassengerReference;
import com.latam.pax.ancillary.offer.PriceElement;
import com.latam.pax.ancillary.offer.ProductDefinition;
import com.latam.pax.ancillary.offer.ReasonForIssuance;
import com.latam.pax.ancillary.offer.ServiceType;
import com.latam.pax.ancillary.offer.BaggageAllowanceOC.MaxPieces;

@RunWith(MockitoJUnitRunner.class)
public class GetAncillaryOffersRSTest {

  private static GetAncillaryOffersRS getAncillaryOffersRS = completeGetAncillaryOffersRS();
  
  /**
   * Test method GetAncillaryOffersRS
   * @return
   */
  public static GetAncillaryOffersRS completeGetAncillaryOffersRS() {
        GetAncillaryOffersRS getAncillaryOffersRS = new GetAncillaryOffersRS();
        
        addGetAncillaryOffers("2","LCO","1","SCL", "LIM",getAncillaryOffersRS, true);
        addGetAncillaryOffers("2","0JO","1","SCL", "LIM",getAncillaryOffersRS, true);
        addGetAncillaryOffers("2","0JO","1","SCL", "LIM",getAncillaryOffersRS, true);
        addGetAncillaryOffers("1","ASV","1","SCL", "LIM",getAncillaryOffersRS, true);
        addGetAncillaryOffers("2","0DF","1","SCL", "LIM",getAncillaryOffersRS, true);
        addGetAncillaryOffers("2","0DF","1","LIM", "SCL",getAncillaryOffersRS, true);
        addGetAncillaryOffers("2","0C3","1","SCL", "LIM",getAncillaryOffersRS, true);
        addGetAncillaryOffers("2","0C3","0","SCL", "LIM",getAncillaryOffersRS, true);
        addGetAncillaryOffers("2","0C3","0","SCL", "LIM",getAncillaryOffersRS, false);
        
        

        return getAncillaryOffersRS;
  }
  
    private static void addGetAncillaryOffers(String typecode, String subCode, String total,
            String origen, String destino, GetAncillaryOffersRS getAncillaryOffersRS, boolean isEquivAmount) {
        
        ProductDefinition productDefinition = setProductDefinition(typecode, subCode);
        Ancillary ancillary = setAncillary(productDefinition);

        getAncillaryOffersRS.getAncillary().add(ancillary);
        getAncillaryOffersRS.getAncillaryDefinition().add(productDefinition);

        Offers offers = setOffers(ancillary, total, origen, destino, isEquivAmount);
        getAncillaryOffersRS.getOffers().add(offers);

        getAncillaryOffersRS.getPassengerOffers().add(setPassengerOffers(offers));
    }
  
  private static ElectronicMiscDocType setElectronicMiscDocType(String typecode) {
      ElectronicMiscDocType electronicMiscDocType = new ElectronicMiscDocType();
      electronicMiscDocType.setCode(typecode);
      electronicMiscDocType.setValue(EnumElectronicMiscDocType.STANDALONE);
      return electronicMiscDocType;
  }
  
  private static ProductDefinition setProductDefinition(String typecode, String subCode) {
      ProductDefinition productDefinition = new ProductDefinition();
      productDefinition.setAirline("LA");
      productDefinition.setSubCode(subCode);
      productDefinition.setCommercialName("value");
      productDefinition.setReasonForIssuance(new ReasonForIssuance());
      productDefinition.getReasonForIssuance().setCode("value");
      productDefinition.getReasonForIssuance().setValue(EnumAncillaryReasonForIssuance.AIR_TRANSPORTATION);
      productDefinition.setElectronicMiscDocType(setElectronicMiscDocType(typecode));
      return productDefinition;
  }
  
  private static Ancillary setAncillary(ProductDefinition productDefinition) {
      Ancillary ancillary = new Ancillary();
      ancillary.setAirExtraStatus("CONFIRMED");
      ancillary.setAncillaryId("id");
      ancillary.setServiceType(new ServiceType());
      ancillary.getServiceType().setValue(EnumServiceTypes.C);
      ancillary.setBaggageAllowance(new BaggageAllowanceOC());
      ancillary.getBaggageAllowance().setMaxPieces(new MaxPieces());
      ancillary.getBaggageAllowance().getMaxPieces().setValue(1);
      ancillary.setAncillaryDefinitionRef(productDefinition);
      return ancillary;
  }
  
  private static Offers.Segment setOffersSegment(){
      Offers.Segment segment = new Offers.Segment();
      segment.setSegmentId("SEG01");
      return segment;
  }
  
  private static Offers setOffers(Ancillary ancillary, String total, String origen, String destino, boolean isEquivAmount) {
      Offers offers = new Offers();
      offers.setOfferId("id");
      offers.setOrigin(origen);
      offers.setDestination(destino);
      offers.setAncillaryRef(ancillary);
      offers.setAncillaryFee(new AncillaryFee());
      offers.getAncillaryFee().setBase(new PriceElement());
      offers.getAncillaryFee().getBase().setEquivAmount(new Money());
      offers.getAncillaryFee().getBase().getEquivAmount().setCurrency("CLP");
      offers.getAncillaryFee().getBase().getEquivAmount().setValue(new BigDecimal("0"));
      offers.getAncillaryFee().getBase().setAmount(new Money());
      offers.getAncillaryFee().getBase().getAmount().setCurrency("CLP");
      offers.getAncillaryFee().getBase().getAmount().setValue(new BigDecimal("0"));
      setTotalAmount(offers, total, isEquivAmount);
      setTaxs(offers, isEquivAmount);
      offers.getSegment().add(setOffersSegment());
      return offers;
  }
  
  private static void setTaxs(Offers offers, boolean isEquivAmount) {
      if(isEquivAmount) {
          offers.getAncillaryFee().getTax().add(new PriceElement());
          offers.getAncillaryFee().getTax().get(0).setCode("1");
          offers.getAncillaryFee().getTax().get(0).setEquivAmount(new Money());
          offers.getAncillaryFee().getTax().get(0).getEquivAmount().setCurrency("CLP");
          offers.getAncillaryFee().getTax().get(0).getEquivAmount().setValue(new BigDecimal("1"));
      } else {
          offers.getAncillaryFee().getTax().add(new PriceElement());
          offers.getAncillaryFee().getTax().get(0).setCode("1");
          offers.getAncillaryFee().getTax().get(0).setAmount(new Money());
          offers.getAncillaryFee().getTax().get(0).getAmount().setCurrency("CLP");
          offers.getAncillaryFee().getTax().get(0).getAmount().setValue(new BigDecimal("1"));
      }
  }
  
  private static void setTotalAmount(Offers offers, String total, boolean isEquivAmount) {
      if(isEquivAmount) {
          offers.getAncillaryFee().setTTLPrice(new PriceElement());
          offers.getAncillaryFee().getTTLPrice().setEquivAmount(new Money());
          offers.getAncillaryFee().getTTLPrice().getEquivAmount().setCurrency("CLP");
          offers.getAncillaryFee().getTTLPrice().getEquivAmount().setValue(new BigDecimal(total));
      } else {
          offers.getAncillaryFee().setTTLPrice(new PriceElement());
          offers.getAncillaryFee().getTTLPrice().setAmount(new Money());
          offers.getAncillaryFee().getTTLPrice().getAmount().setCurrency("CLP");
          offers.getAncillaryFee().getTTLPrice().getAmount().setValue(new BigDecimal(total));
      }
  }
  
  private static PassengerOffers setPassengerOffers(Offers offers) {
      PassengerOffers passengerOffers = new PassengerOffers();
      passengerOffers.setPassengerReference(new PassengerReference());
      passengerOffers.getPassengerReference().setNameAssociationId("01.01");
      passengerOffers.getPassengerReference().setNameReferenceNumber("01.01");
      passengerOffers.getPassengerReference().setPassengerId("1");
      passengerOffers.getOfferRefs().add(offers);
      return passengerOffers;
  }
  
  @Test
  public void testParameters(){
    Assert.assertNotNull(ConstantsTest.MUST_BE_A_NOTNULL, getAncillaryOffersRS);
  }
  

}
