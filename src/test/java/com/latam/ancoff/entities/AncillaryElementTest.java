package com.latam.ancoff.entities;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import com.latam.ancoff.entities.utils.ConstantsTest;

@RunWith(MockitoJUnitRunner.class)
public class AncillaryElementTest {
    
    private static final String PAX1 = "01.01";
    private static final String PAX2 = "02.01";

    private static AncillaryElement ancillaryElement = completeAncillaryElement("0C3",PAX1,"CCP","SCL","1");
    
    /**
     * Test method GetAncillaryOffersRQ
     * @return
     */
    public static AncillaryElement completeAncillaryElement(String subCode, String paxNameNumber,
            String departureAirport, String arrivalAirport, String segmentNumber) {
        AncillaryElement ancillaryElement = new AncillaryElement();
        ancillaryElement.setSubCode(subCode);
        Passenger passenger = new Passenger();
        passenger.setPaxNameNumber(paxNameNumber);
        ancillaryElement.setPassenger(passenger);
        ancillaryElement.setPortions(new Portions());
        ancillaryElement.getPortions().setPortion(new ArrayList<Portion>());
        Portion portion = new Portion();
        portion.setDepartureAirport(departureAirport);
        portion.setArrivalAirport(arrivalAirport);
        portion.setSegmentNumber(segmentNumber);
        ancillaryElement.getPortions().getPortion().add(portion);
        return ancillaryElement;
    }
    
    public static List<AncillaryElement> completeListAncillaryElement() {
        List<AncillaryElement> list = new ArrayList<>();
        list.add(completeAncillaryElement("0C3",PAX1,"CCP","SCL","1"));
        list.add(completeAncillaryElement("0JO",PAX1,"CCP","SCL","1"));
        list.add(completeAncillaryElement("0JT",PAX1,"CCP","SCL","1"));
        list.add(completeAncillaryElement("0DF",PAX1,"SCL","LIM","2"));
        list.add(completeAncillaryElement("0DF",PAX2,"CCP","SCL","1"));
        list.add(completeAncillaryElement("0C3",PAX2,"SCL","LIM","2"));
        list.add(completeAncillaryElement("0MQ",PAX2,"SCL","LIM","2"));
        list.add(completeAncillaryElement("0C3",PAX2,"SCL","LIM",null));
        list.add(completeAncillaryElement("0JO",PAX2,"CCP","SCL","1"));
        list.add(completeAncillaryElement("0XX",PAX2,"CCP","SCL","1"));
        return list;
    }
    
    @Test
    public void testParameters(){
      Assert.assertNotNull(ConstantsTest.MUST_BE_A_NOTNULL, ancillaryElement);
    }
    

}
