package com.latam.ancoff.entities.utils;

import org.junit.Test;
import java.lang.reflect.Constructor;

import static org.junit.Assert.assertFalse;

public class ConstantsTest {
  public static final String MUST_BE_A_TRUE = "expected a true value";
  public static final String MUST_BE_A_FALSE = "expected a false value";
  public static final String MUST_BE_A_NULL = "expected a null value";
  public static final String MUST_BE_A_NOTNULL = "can't be a null value";
  public static final String MUST_BE_A_EQUALS = "expected value equals to '%s'";
  public static final String MUST_BE_A_NOTEQUALS = "expected value not equals to '%s'";

  @Test
  public void testContantsTest() throws NoSuchMethodException {
    Constructor<ConstantsTest> duConts = ConstantsTest.class.getDeclaredConstructor();
    assertFalse(MUST_BE_A_FALSE, duConts.isAccessible());
  }
}
