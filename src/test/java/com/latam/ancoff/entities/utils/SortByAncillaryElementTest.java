package com.latam.ancoff.entities.utils;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.latam.ancoff.entities.AncillaryElementTest;
import com.latam.ancoff.utils.SortByAncillaryElement;
import com.latam.ancoff.utils.SortOtherBaggage;

import lombok.extern.slf4j.Slf4j;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class SortByAncillaryElementTest {
    
    SortByAncillaryElement sort;
    boolean result = true;
    private static final String MENSAJE = "mensaje de prueba";
    
    @Before
    public void beforeData() {
        sort = new SortByAncillaryElement("0DF,0C3,0JT,0JO");
    }
    
    @Test
    public void testAncillaryOfferRequest() {
        try {
        AncillaryElementTest.completeListAncillaryElement().sort(sort);
        AncillaryElementTest.completeListAncillaryElement().sort(new SortOtherBaggage());
            assertTrue(MENSAJE, result);
        }catch (Exception e) {
            log.info("Error testMapperAncillaryOfferRS : ", e);
        }
       
        
    }

}
