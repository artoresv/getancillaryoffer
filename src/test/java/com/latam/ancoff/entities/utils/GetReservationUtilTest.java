package com.latam.ancoff.entities.utils;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import com.latam.ancoff.utils.GetReservationUtil;
import com.latam.host.sabre.ws.WSSabreSessionManager;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class GetReservationUtilTest {

    private static final String MENSAJE = "mensaje de prueba";
    @Test
    public void testPartyId() {

        WSSabreSessionManager wsSession = mock(WSSabreSessionManager.class);
        when(wsSession.getTo()).thenReturn("To");
        when(wsSession.getFrom()).thenReturn("From");
        when(wsSession.getBinarySecurityToken()).thenReturn("12345656896575");
        assertNotNull(MENSAJE, GetReservationUtil.getReservationGetParty(wsSession));
        assertNotNull(MENSAJE, GetReservationUtil.getReservationGetPartyId(wsSession));
        assertNotNull(MENSAJE, GetReservationUtil.getReservationGetSecurity(wsSession));
        assertNotNull(MENSAJE, GetReservationUtil.getReservationGetMessageData());
    }

}
