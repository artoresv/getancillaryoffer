package com.latam.ancoff.entities.utils;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.latam.ancoff.entities.AncOfferRQ;
import com.latam.ancoff.entities.AncOfferRS;
import com.latam.ancoff.entities.AncillaryElement;
import com.latam.ancoff.entities.AncillaryElements;
import com.latam.ancoff.utils.AppProperties;
import com.latam.ancoff.utils.MapperResponseUtilBaggage;
import com.latam.ancoff.entities.AncOfferRQTest;
import com.latam.ancoff.entities.AncillaryElementTest;

import lombok.extern.slf4j.Slf4j;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class MapperResponseUtilBaggageTest {

    AppProperties appProperties = new AppProperties();

    private static final String MENSAJE = "mensaje de prueba";
    boolean result = true;
    
    @Before
    public void before() {
        appProperties.setBaggageOrderList("0DF,0C3,0JT,0JO");
    }


    @Test
    public void testMapperBaggage() {

        try {
            AncOfferRS ancOfferRS = new AncOfferRS();
            ancOfferRS.setGetAncillaryOffersRS(new com.latam.ancoff.entities.GetAncillaryOffersRS());
            ancOfferRS.getGetAncillaryOffersRS().setAncillaryElements(new AncillaryElements());
            ancOfferRS.getGetAncillaryOffersRS().getAncillaryElements().setAncillaryElement(new ArrayList<AncillaryElement>());
            List<AncillaryElement> list = AncillaryElementTest.completeListAncillaryElement();
            ancOfferRS.getGetAncillaryOffersRS().getAncillaryElements().getAncillaryElement().addAll(list);
            AncOfferRQ ancOfferRQ = AncOfferRQTest.completeFilterAncOfferRQ("false","BG");
            
            MapperResponseUtilBaggage.isSortAncillaryElement(ancOfferRQ, ancOfferRS, appProperties);
            MapperResponseUtilBaggage.setNumberBaggage(ancOfferRQ, list);
            assertTrue(MENSAJE, result);
        } catch (Exception e) {
            log.info("Error testMapperBaggage : ", e);
        }

    }

}
