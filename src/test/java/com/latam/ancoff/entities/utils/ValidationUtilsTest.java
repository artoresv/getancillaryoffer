package com.latam.ancoff.entities.utils;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.latam.ancoff.entities.AncOfferRS;
import com.latam.ancoff.utils.ValidationUtils;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ValidationUtilsTest {
    
    boolean value;
    AncOfferRS ancOfferRS;
    private static final String MENSAJE = "mensaje de prueba";
    
    @Before
    public void beforeData() {
        ancOfferRS = new AncOfferRS();
    }

    @Test
    public void testValidation() {
      value = ValidationUtils.validateStringNotNull(null);
      assertTrue(MENSAJE, value);
      value = ValidationUtils.validateStringNotNull("");
      assertTrue(MENSAJE, value);
      value = ValidationUtils.validateStringNotNull(Double.valueOf(0));
      assertTrue(MENSAJE, value);
      value = ValidationUtils.validateStringNotNull(Integer.valueOf(0));
      assertTrue(MENSAJE, value);
    }
    
    @Test
    public void testValidateException() {
        value = true;
      assertTrue(MENSAJE, value);
    }
    
}
