package com.latam.ancoff.entities.utils;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.latam.ancoff.entities.AncOfferRQ;
import com.latam.ancoff.entities.AncOfferRQTest;
import com.latam.ancoff.entities.AncOfferRS;
import com.latam.ancoff.entities.GetReservationResponse;
import com.latam.ancoff.utils.AppProperties;
import com.latam.ancoff.utils.MapperResponseUtil;
import com.latam.ancoff.entities.GetAncillaryOffersRQTest;
import com.latam.ancoff.entities.GetAncillaryOffersRSTest;
import com.latam.ancoff.entities.GetReservationResponseTest;
import com.latam.pax.ancillary.offer.EnumServiceTypes;
import com.latam.pax.ancillary.offer.GetAncillaryOffersRQ;
import com.latam.pax.ancillary.offer.GetAncillaryOffersRS;
import com.latam.pax.getreservation.PassengerTypePNRB;

import lombok.extern.slf4j.Slf4j;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class MapperResponseUtilTest {

    AppProperties appProperties = new AppProperties();

    private static final String MENSAJE = "mensaje de prueba";
    boolean result = true;
    
    @Before
    public void before() {
        appProperties.setNoAncillaryElement("NO_AE");
        appProperties.setTypeCodeInfoPortion("2");
    }


    @Test
    public void testMapperAncillaryOfferRS() {

        try {

            GetAncillaryOffersRS getAncillaryOffersRS = GetAncillaryOffersRSTest.completeGetAncillaryOffersRS();
            GetAncillaryOffersRQ getAncillaryOffersRQ = GetAncillaryOffersRQTest.completeGetAncillaryOffersRQ();
            GetReservationResponse reservationRS = GetReservationResponseTest.completeGetReservationResponse(null,
                    PassengerTypePNRB.S, true, "QLESLH6B", false);
            
            AncOfferRQ ancOfferRQ = AncOfferRQTest.completeFilterAncOfferRQ("true","PT");
            AncOfferRQ ancOfferFalseRQ = AncOfferRQTest.completeFilterAncOfferRQ("false","PT");
            AncOfferRS ancOfferRS = new AncOfferRS();

            MapperResponseUtil.mapperAncOfferRS(ancOfferRQ, ancOfferRS, getAncillaryOffersRS, getAncillaryOffersRQ, reservationRS,
                    appProperties);
            getAncillaryOffersRS.getAncillaryDefinition().get(0).setSubCode("0JO");
            getAncillaryOffersRS.getAncillaryDefinition().get(0).getElectronicMiscDocType().setCode("1");
            MapperResponseUtil.mapperAncOfferRS(ancOfferRQ, ancOfferRS, getAncillaryOffersRS, getAncillaryOffersRQ, reservationRS,
                    appProperties);
            getAncillaryOffersRS.getAncillaryDefinition().get(0).setSubCode("0C3");
            MapperResponseUtil.mapperAncOfferRS(ancOfferRQ, ancOfferRS, getAncillaryOffersRS, getAncillaryOffersRQ, reservationRS,
                    appProperties);
            getAncillaryOffersRS.getAncillaryDefinition().get(0).setSubCode("0C3");
            getAncillaryOffersRS.getAncillary().get(0).getServiceType().setValue(EnumServiceTypes.A);
            MapperResponseUtil.mapperAncOfferRS(ancOfferRQ, ancOfferRS, getAncillaryOffersRS, getAncillaryOffersRQ, reservationRS,
                    appProperties);
            getAncillaryOffersRS.getAncillaryDefinition().get(0).setGroup("BG");
            getAncillaryOffersRS.getAncillaryDefinition().get(0).setSubCode("0DF");
            MapperResponseUtil.mapperAncOfferRS(ancOfferRQ, ancOfferRS, getAncillaryOffersRS, getAncillaryOffersRQ, reservationRS,
                    appProperties);
            getAncillaryOffersRS.getAncillaryDefinition().get(0).setSubCode("0C3");
            MapperResponseUtil.mapperAncOfferRS(ancOfferRQ, ancOfferRS, getAncillaryOffersRS, getAncillaryOffersRQ, reservationRS,
                    appProperties);
            getAncillaryOffersRS.getAncillaryDefinition().get(0).setSubCode("0DF");
            getAncillaryOffersRS.getOffers().get(0).getAncillaryFee().getBase().setEquivAmount(null);
            MapperResponseUtil.mapperAncOfferRS(ancOfferRQ, ancOfferRS, getAncillaryOffersRS, getAncillaryOffersRQ, reservationRS,
                    appProperties);
            getAncillaryOffersRS.getAncillaryDefinition().get(0).setSubCode("0DF");
            getAncillaryOffersRS.getOffers().get(0).getAncillaryFee().getBase().setAmount(null);
            getAncillaryOffersRS.getOffers().get(0).getAncillaryFee().getBase().setEquivAmount(null);
            getAncillaryOffersRS.getOffers().get(0).getAncillaryFee().getTTLPrice().setEquivAmount(null);
            MapperResponseUtil.mapperAncOfferRS(ancOfferRQ, ancOfferRS, getAncillaryOffersRS, getAncillaryOffersRQ, reservationRS,
                    appProperties);
            getAncillaryOffersRS.getAncillaryDefinition().get(0).setSubCode("0DF");
            getAncillaryOffersRS.getOffers().get(0).getAncillaryFee().setBase(null);
            MapperResponseUtil.mapperAncOfferRS(ancOfferRQ, ancOfferRS, getAncillaryOffersRS, getAncillaryOffersRQ, reservationRS,
                    appProperties);
            getAncillaryOffersRS.getAncillaryDefinition().get(0).setGroup("BG");
            getAncillaryOffersRS.getAncillaryDefinition().get(0).setSubCode("0DF");
            getAncillaryOffersRS.getOffers().get(0).getAncillaryFee().setBase(null);
            MapperResponseUtil.mapperAncOfferRS(ancOfferFalseRQ, ancOfferRS, getAncillaryOffersRS, getAncillaryOffersRQ, reservationRS,
                    appProperties);
            MapperResponseUtil.mapperAncOfferRS(ancOfferRQ, ancOfferRS, null, getAncillaryOffersRQ, reservationRS, appProperties);
            assertTrue(MENSAJE, result);
        } catch (Exception e) {
            log.info("Error testMapperAncillaryOfferRS : ", e);
        }

    }

}
