package com.latam.ancoff.entities.utils;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.latam.ancoff.entities.AncOfferRQTest;
import com.latam.ancoff.entities.GetAncillaryOffersRQTest;
import com.latam.ancoff.entities.GetReservationResponseTest;
import com.latam.ancoff.entities.AncOfferRQ;
import com.latam.ancoff.utils.AppProperties;
import com.latam.ancoff.utils.MapperGetAncillaryOffer;
import com.latam.pax.ancillary.offer.GetAncillaryOffersRQ;
import com.latam.pax.getreservation.PassengerTypePNRB;

import lombok.extern.slf4j.Slf4j;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class MapperGetAncillaryOfferTest {
    
    AppProperties appProperties = new AppProperties();
    
    private static final String MENSAJE = "mensaje de prueba";
    private static final String FAREBASIS_RS = "QLESLH6B";
    private static final String FAREBASIS_RS_NOFARE = "YEESFZ0B";
    private static final String FAREBASIS_RS_NOFARE_LEN = "YEESFZ0BOT";
    boolean result = true;
    
    @Before
    public void beforeData() {
        appProperties.setGetAncillaryOfferVersion("1.0");
        appProperties.setAncillaryRequestOptionsGroup("BG,99,IE,TS,UN,UP,PT,MD");
    }

    @Test
    public void testAncillaryOfferRequest() {
        try {
            AncOfferRQ ancOfferRQ = new AncOfferRQ();
            AncOfferRQ ancFilterOfferRQ = AncOfferRQTest.completeFilterAncOfferRQ("true","PT");

            GetAncillaryOffersRQ getAncillaryOffersRQ = GetAncillaryOffersRQTest.completeGetAncillaryOffersRQ();

            MapperGetAncillaryOffer.getRequest(getAncillaryOffersRQ,
                    GetReservationResponseTest.completeGetReservationResponse(null, PassengerTypePNRB.S, true, FAREBASIS_RS, false),
                    ancOfferRQ, appProperties);
            MapperGetAncillaryOffer.getRequest(getAncillaryOffersRQ,
                    GetReservationResponseTest.completeGetReservationResponse("CNN", PassengerTypePNRB.S, true, FAREBASIS_RS, false),
                    ancOfferRQ, appProperties);
            MapperGetAncillaryOffer.getRequest(getAncillaryOffersRQ,
                    GetReservationResponseTest.completeGetReservationResponse("CNN", PassengerTypePNRB.S, true, FAREBASIS_RS, false),
                    ancFilterOfferRQ, appProperties);
            MapperGetAncillaryOffer.getRequest(getAncillaryOffersRQ,
                    GetReservationResponseTest.completeGetReservationResponse("INF", PassengerTypePNRB.I, true, FAREBASIS_RS, false),
                    ancOfferRQ, appProperties);
            MapperGetAncillaryOffer.getRequest(getAncillaryOffersRQ,
                    GetReservationResponseTest.completeGetReservationResponse("INF", PassengerTypePNRB.I, false, FAREBASIS_RS, false),
                    ancOfferRQ, appProperties);
            appProperties.setAncillaryRequestOptionsGroup("");
            MapperGetAncillaryOffer.getRequest(getAncillaryOffersRQ,
                    GetReservationResponseTest.completeGetReservationResponse("ADT", PassengerTypePNRB.S, true, FAREBASIS_RS, false),
                    ancOfferRQ, appProperties);
            appProperties.setAncillaryRequestOptionsGroup(null);
            MapperGetAncillaryOffer.getRequest(getAncillaryOffersRQ,
                    GetReservationResponseTest.completeGetReservationResponse("ADT", PassengerTypePNRB.S, true, FAREBASIS_RS, false),
                    ancOfferRQ, appProperties);
            appProperties.setAncillaryRequestOptionsGroup(null);
            MapperGetAncillaryOffer.getRequest(getAncillaryOffersRQ,
                    GetReservationResponseTest.completeGetReservationResponse("ADT", PassengerTypePNRB.S, true, FAREBASIS_RS_NOFARE_LEN, false),
                    ancOfferRQ, appProperties);
            appProperties.setAncillaryRequestOptionsGroup(null);
            MapperGetAncillaryOffer.getRequest(getAncillaryOffersRQ,
                    GetReservationResponseTest.completeGetReservationResponse("ADT", PassengerTypePNRB.S, true, FAREBASIS_RS_NOFARE, true),
                    ancOfferRQ, appProperties);
            MapperGetAncillaryOffer.getRequest(getAncillaryOffersRQ,
                    GetReservationResponseTest.completeGetReservationResponse("ADT", PassengerTypePNRB.S, true, FAREBASIS_RS_NOFARE_LEN, true),
                    ancOfferRQ, appProperties);
            assertTrue(MENSAJE, result);
        } catch (Exception e) {
            log.info("Error testAncillaryOfferRequest : ", e);
        }

    }
    
}
