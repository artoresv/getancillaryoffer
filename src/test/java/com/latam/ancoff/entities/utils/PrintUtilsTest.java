package com.latam.ancoff.entities.utils;

import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.latam.ancoff.utils.PrintUtil;
import com.latam.pax.ancillary.offer.GetAncillaryOffersRQ;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PrintUtilsTest {
    
    GetAncillaryOffersRQ request;
    private static final String MENSAJE = "mensaje de prueba";
    @Before
    public void beforeData() {
        request = new GetAncillaryOffersRQ();
        request.setVersion("1.0");
    }

    @Test
    public void testPrint() {
      assertNotNull(MENSAJE, PrintUtil.jaxbToString(request));

    }
    
    @Test
    public void testPrintError() {
      assertNotNull(MENSAJE, PrintUtil.jaxbToString("<xml>error</xml>"));
    }
    
    @Test
    public void testPrintNull() {
      assertNotNull(MENSAJE, PrintUtil.jaxbToString(null));
    }
    
}
