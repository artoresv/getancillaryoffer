package com.latam.ancoff.entities.utils;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import com.latam.ancoff.utils.Constants;
import com.latam.ancoff.utils.HealthCheckUtil;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class HealthCheckUtilTest {

    HealthCheckUtil healthCheckUtil = new HealthCheckUtil();
    private static final String MENSAJE = "mensaje de prueba";

    @Test
    public void testCheckHttpStatus() {

        assertTrue(MENSAJE, HealthCheckUtil.validHttpStatus(0));
        assertTrue(MENSAJE, HealthCheckUtil.validHttpStatus(Constants.HTTP_STATUS_UNAVAILABLE));
        assertTrue(MENSAJE, HealthCheckUtil.validHttpStatus(Constants.HTTP_STATUS_404));

    }

}
