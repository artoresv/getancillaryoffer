package com.latam.ancoff.entities;


import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import com.latam.ancoff.entities.utils.ConstantsTest;
import com.latam.pax.ancillary.offer.ClientContext;
import com.latam.pax.ancillary.offer.FareBreakAssociation;
import com.latam.pax.ancillary.offer.FareInfo;
import com.latam.pax.ancillary.offer.Flight;
import com.latam.pax.ancillary.offer.GetAncillaryOffersRQ;
import com.latam.pax.ancillary.offer.LoyaltyAccounts;
import com.latam.pax.ancillary.offer.Passenger;
import com.latam.pax.ancillary.offer.PassengerSegment;
import com.latam.pax.ancillary.offer.RequestType;
import com.latam.pax.ancillary.offer.Segment;
import com.latam.pax.ancillary.offer.GetAncillaryOffersRQ.QueryByItinerary;
import com.latam.pax.ancillary.offer.GetAncillaryOffersRQ.QueryByItinerary.QueryPassengerItinerary;
import com.latam.pax.ancillary.offer.GetAncillaryOffersRQ.QueryByItinerary.QueryPassengerItinerary.PassengerItinerary;
import com.latam.pax.ancillary.offer.LoyaltyAccounts.LoyaltyAccountWithId;

import lombok.extern.slf4j.Slf4j;

@RunWith(MockitoJUnitRunner.class)
@Slf4j
public class GetAncillaryOffersRQTest {

  private static GetAncillaryOffersRQ getAncillaryOffersRQ = completeGetAncillaryOffersRQ();
  
  /**
   * Test method GetAncillaryOffersRQ
   * @return
   */
  public static GetAncillaryOffersRQ completeGetAncillaryOffersRQ() {
      GetAncillaryOffersRQ getAncillaryOffersRQ = new GetAncillaryOffersRQ();
      
      getAncillaryOffersRQ.setVersion("1.1");
      getAncillaryOffersRQ.setRequestType(RequestType.PAYLOAD);
      getAncillaryOffersRQ.setRequestMode("booking");
      
      setClientContext(getAncillaryOffersRQ);

      /*** QueryByItinerary ***/
      QueryByItinerary queryByItinerary = new QueryByItinerary();

      Segment segmentOne = setSegmentOne();
      Segment segmentTwo = setSegmentTwo();

      FareInfo fareInfoOne = new FareInfo();
      fareInfoOne.setId("FareID1");
      fareInfoOne.setFareBasisCode("MLESL00I");
      queryByItinerary.getFareInfo().add(fareInfoOne);
      
      FareInfo fareInfoTwo = new FareInfo();
      fareInfoTwo.setId("FareID2");
      fareInfoTwo.setFareBasisCode("YLESFZ0I");
      queryByItinerary.getFareInfo().add(fareInfoTwo);
      
      setQueryPassengerItineraryOne(queryByItinerary, 
              segmentOne, segmentTwo, fareInfoOne, fareInfoTwo);
      setQueryPassengerItineraryTwo(queryByItinerary, 
              segmentOne, segmentTwo, fareInfoOne, fareInfoTwo);

      queryByItinerary.getSegment().add(segmentOne);
      queryByItinerary.getSegment().add(segmentTwo);
      getAncillaryOffersRQ.setQueryByItinerary(queryByItinerary);
      

      return getAncillaryOffersRQ;
  }
  
  private static void setClientContext(GetAncillaryOffersRQ getAncillaryOffersRQ) {
      /*** ClientContext ***/
      ClientContext clientContext = new ClientContext();
      clientContext.setClientType("");
      clientContext.setCityCode("VAP");
      clientContext.setPCC("VAP");
      clientContext.setAirlineCarrierCode("LA");
      clientContext.setAgentCurrencyCode("CLP");
      getAncillaryOffersRQ.setClientContext(clientContext);
  }
  
  private static Segment setSegmentOne() {
      Segment segment = new Segment();
      segment.setId("SEG01");
      segment.setSegmentNumber("1");
      Flight flight = new Flight();
      flight.setId("flight_1");
      flight.setAirline("LA");
      flight.setFlightNumber("8015");
      flight.setDepartureAirport("EZE");
      flight.setArrivalAirport("GRU");

      XMLGregorianCalendar departureDate = null;
      XMLGregorianCalendar arrivalDate = null;
      XMLGregorianCalendar departureTime = null;
      XMLGregorianCalendar arrivalTime = null;
      try {
          departureDate = DatatypeFactory.newInstance().newXMLGregorianCalendar("2019-04-20T06:33:00-00:00");
          departureTime = DatatypeFactory.newInstance().newXMLGregorianCalendar("2019-04-20T06:33:00-00:00");
          flight.setDepartureDate(departureDate);
          flight.setDepartureTime(departureTime);
          arrivalDate = DatatypeFactory.newInstance().newXMLGregorianCalendar("2019-04-20T09:30:00-00:00");
          arrivalTime = DatatypeFactory.newInstance().newXMLGregorianCalendar("2019-04-20T09:30:00-00:00");
          flight.setArrivalDate(arrivalDate);
          flight.setArrivalTime(arrivalTime);
      } catch (DatatypeConfigurationException e) {
          log.info("Error DateSegment : ", e);
      }
      flight.setOperatingAirline("LA");
      flight.setOperatingFlightNumber("8015");
      flight.setClassOfService("M");
      segment.setFlightDetail(flight);
      segment.setInboundConnection(false);
      segment.setOutboundConnection(true);
      return segment;
  }
  
  private static Segment setSegmentTwo() {
      Segment segment2 = new Segment();
      segment2.setId("SEG02");
      segment2.setSegmentNumber("2");
      Flight flight2 = new Flight();
      flight2.setId("flight_2");
      flight2.setAirline("LA");
      flight2.setFlightNumber("7869");
      flight2.setDepartureAirport("GRU");
      flight2.setArrivalAirport("AEP");

      XMLGregorianCalendar departureDate2 = null;
      XMLGregorianCalendar arrivalDate2 = null;
      XMLGregorianCalendar departureTime2 = null;
      XMLGregorianCalendar arrivalTime2 = null;
      try {
          departureDate2 = DatatypeFactory.newInstance().newXMLGregorianCalendar("2019-04-25T18:25:00-00:00");
          departureTime2 = DatatypeFactory.newInstance().newXMLGregorianCalendar("2019-04-25T18:25:00-00:00");
          flight2.setDepartureDate(departureDate2);
          flight2.setDepartureTime(departureTime2);
          arrivalDate2 = DatatypeFactory.newInstance().newXMLGregorianCalendar("2019-04-25T21:15:00-00:00");
          arrivalTime2 = DatatypeFactory.newInstance().newXMLGregorianCalendar("2019-04-25T21:15:00-00:00");
          flight2.setArrivalDate(arrivalDate2);
          flight2.setArrivalTime(arrivalTime2);
      } catch (DatatypeConfigurationException e) {
          log.info("Error DateSegment : ", e);
      }
      flight2.setOperatingAirline("LA");
      flight2.setOperatingFlightNumber("7869");
      flight2.setClassOfService("Y");
      segment2.setFlightDetail(flight2);
      segment2.setInboundConnection(true);
      segment2.setOutboundConnection(false);
      return segment2;
  }
  
  private static void setQueryPassengerItineraryOne(QueryByItinerary queryByItinerary, 
          Segment segmentOne, Segment segmentTwo, FareInfo fareInfoOne, FareInfo fareInfoTwo) {
      
      QueryPassengerItinerary queryPassengerItinerary = new QueryPassengerItinerary();
      PassengerItinerary passengerItinerary = new PassengerItinerary();
      Passenger passenger = new Passenger();
      passenger.setId("p1");
      passenger.setNameReferenceNumber("01.01");
      passenger.setType("ADULT");
      passenger.setLoyaltyAccounts(new LoyaltyAccounts());
      LoyaltyAccountWithId loyaltyAccountWithId = new LoyaltyAccountWithId();
      loyaltyAccountWithId.setMemberId("12345678");
      passenger.getLoyaltyAccounts().getLoyaltyAccountWithId().add(loyaltyAccountWithId);
      //1 fare y seg
      PassengerSegment passengerSegment = new PassengerSegment();
      passengerSegment.setSegmentRef(segmentOne);
      FareBreakAssociation fareBreakAssociation = new FareBreakAssociation();
      fareBreakAssociation.setFareInfoRef(fareInfoOne);
      passengerSegment.getFareBreakAssociation().add(fareBreakAssociation);
      passengerItinerary.getPassengerSegment().add(passengerSegment);
      //2 fare y seg
      passengerSegment = new PassengerSegment();
      passengerSegment.setSegmentRef(segmentTwo);
      fareBreakAssociation = new FareBreakAssociation();
      fareBreakAssociation.setFareInfoRef(fareInfoTwo);
      passengerSegment.getFareBreakAssociation().add(fareBreakAssociation);
      passengerItinerary.getPassengerSegment().add(passengerSegment);
      
      queryPassengerItinerary.setPassenger(passenger);
      queryPassengerItinerary.getPassengerItinerary().add(passengerItinerary);
      queryByItinerary.getQueryPassengerItinerary().add(queryPassengerItinerary);
  }
  
  private static void setQueryPassengerItineraryTwo(QueryByItinerary queryByItinerary, 
          Segment segmentOne, Segment segmentTwo, FareInfo fareInfoOne, FareInfo fareInfoTwo) {
      QueryPassengerItinerary queryPassengerItinerary2 = new QueryPassengerItinerary();
      PassengerItinerary passengerItinerary2 = new PassengerItinerary();
      Passenger passenger2 = new Passenger();
      passenger2.setId("p2");
      passenger2.setNameReferenceNumber("02.01");
      passenger2.setType("CHILD");
      
      //1 fare y seg
      PassengerSegment passengerSegment2 = new PassengerSegment();
      passengerSegment2.setSegmentRef(segmentOne);
      FareBreakAssociation fareBreakAssociation2 = new FareBreakAssociation();
      fareBreakAssociation2.setFareInfoRef(fareInfoOne);
      passengerSegment2.getFareBreakAssociation().add(fareBreakAssociation2);
      passengerItinerary2.getPassengerSegment().add(passengerSegment2);
      //2 fare y seg
      passengerSegment2 = new PassengerSegment();
      passengerSegment2.setSegmentRef(segmentTwo);
      fareBreakAssociation2 = new FareBreakAssociation();
      fareBreakAssociation2.setFareInfoRef(fareInfoTwo);
      passengerSegment2.getFareBreakAssociation().add(fareBreakAssociation2);
      passengerItinerary2.getPassengerSegment().add(passengerSegment2);
      
      queryPassengerItinerary2.setPassenger(passenger2);
      queryPassengerItinerary2.getPassengerItinerary().add(passengerItinerary2);
      queryByItinerary.getQueryPassengerItinerary().add(queryPassengerItinerary2);

  }
  
  @Test
  public void testParameters(){
    Assert.assertNotNull(ConstantsTest.MUST_BE_A_NOTNULL, getAncillaryOffersRQ);
  }
  

}
