package com.latam.ancoff.entities.impl;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import com.latam.ancoff.entities.AncOfferRQ;
import com.latam.ancoff.entities.AncOfferRQTest;
import com.latam.ancoff.entities.AncOfferRS;
import com.latam.ancoff.entities.GetAncillaryOffersRS;
import com.latam.ancoff.entities.ServiceStatus;
import com.latam.ancoff.impl.AncillaryOfferImpl;
import com.latam.ancoff.utils.AppProperties;
import com.latam.host.sabre.ws.WSSabreSessionManager;

import lombok.extern.slf4j.Slf4j;

@RunWith(MockitoJUnitRunner.Silent.class)
@SpringBootTest
@Slf4j
public class AncillaryOfferImplTest {
    
    @InjectMocks
    AncillaryOfferImpl ancillaryOfferImpl = new AncillaryOfferImpl();
    
    @Mock
    WSSabreSessionManager ssm;
    @Mock
    AppProperties appProperties = new AppProperties();

    
    String appName = "Payfields";
    private static final String MENSAJE = "mensaje de prueba";
    boolean result = true;
    
    @Before
    public void setup() {
        when(appProperties.getSessionCreateEndpoint())
                .thenReturn("http://ldsvsoaap04.cl.lan.com:12102/WSSessionManager-4.0-webservice/SessionCreate");
        when(appProperties.getSessionCloseEndpoint())
                .thenReturn("http://ldsvsoaap04.cl.lan.com:12102/WSSessionManager-4.0-webservice/SessionClose");
        when(appProperties.getAirlineCode())
                .thenReturn("LA");
        
        when(ssm.getBinarySecurityToken()).thenReturn("3456789975433");

    }
    
    @Test
    public void testProcess() {
        AncOfferRQ ancOfferRQ = AncOfferRQTest.completeAncOfferRQ();
        
        try {
            ancillaryOfferImpl.process(appName, null, ancOfferRQ);
            assertTrue(MENSAJE, result);
        } catch (Exception e) {
            log.info("Error testProcess : ", e);
        }
        
    }
    
    @Test
    public void testServiceSSID() {
        AncOfferRQ ancOfferRQ = AncOfferRQTest.completeAncOfferRQ();
        
        try {
            ancillaryOfferImpl.process(appName, "990041-0:0:0:0:0:0:0:1", ancOfferRQ);
            assertTrue(MENSAJE, result);
        } catch (Exception e) {
            log.info("Error testServiceSSID : ", e);
        }
        
    }
    
    @Test
    public void testLogic() {

        AncOfferRS ancOfferRS = new AncOfferRS();

        try {
            ancillaryOfferImpl.ancillaryLogic(AncOfferRQTest.completeAncOfferRQPnrIncompleto(), ancOfferRS, ssm);
            ancOfferRS = new AncOfferRS();
            ancOfferRS.setGetAncillaryOffersRS(new GetAncillaryOffersRS());
            ancOfferRS.getGetAncillaryOffersRS().setServiceStatus(new ServiceStatus());
            ancOfferRS.getGetAncillaryOffersRS().getServiceStatus().setCode(-1);
            ancOfferRS.getGetAncillaryOffersRS().getServiceStatus().setMessage("ErrorMessage");
            ancOfferRS.getGetAncillaryOffersRS().getServiceStatus().setNativeMessage("ErrorNativeMessage");
            ancillaryOfferImpl.ancillaryLogic(AncOfferRQTest.completeAncOfferRQ(), ancOfferRS, ssm);

            assertTrue(MENSAJE, result);
        } catch (Exception e) {
            log.info("Error testServiceSSID : ", e);
        }
        
    }
    

}
