package com.latam.ancoff.entities;


import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import com.latam.ancoff.entities.AgentPos;
import com.latam.ancoff.entities.AncOfferRQ;
import com.latam.ancoff.entities.AncillaryElement;
import com.latam.ancoff.entities.AncillaryElements;
import com.latam.ancoff.entities.Filter;
import com.latam.ancoff.entities.Reservation;
import com.latam.ancoff.entities.utils.ConstantsTest;

@RunWith(MockitoJUnitRunner.class)
public class AncOfferRQTest {

  private static AncOfferRQ ancOfferRQ = completeAncOfferRQ();
  
  /**
   * Test method GetAncillaryOffersRQ
   * @return
   */
  public static AncOfferRQ completeAncOfferRQ() {
      AncOfferRQ ancOfferRQ = new AncOfferRQ();
      ancOfferRQ.setGetAncillaryOffersRQ(new com.latam.ancoff.entities.GetAncillaryOffersRQ());
      ancOfferRQ.getGetAncillaryOffersRQ().setAgentPos(new AgentPos());
      ancOfferRQ.getGetAncillaryOffersRQ().getAgentPos().setPcc("EYN");
      ancOfferRQ.getGetAncillaryOffersRQ().setIsoCurrencyCode("CLP");
      ancOfferRQ.getGetAncillaryOffersRQ().setReservation(new Reservation());
      ancOfferRQ.getGetAncillaryOffersRQ().getReservation().setRecordLocator("ZNATOG");
      return ancOfferRQ;
  }
  
  public static AncOfferRQ completeAncOfferRQFilterPT() {
      AncOfferRQ ancOfferRQ = new AncOfferRQ();
      ancOfferRQ.setGetAncillaryOffersRQ(new com.latam.ancoff.entities.GetAncillaryOffersRQ());
      ancOfferRQ.getGetAncillaryOffersRQ().setAgentPos(new AgentPos());
      ancOfferRQ.getGetAncillaryOffersRQ().getAgentPos().setPcc("EYN");
      ancOfferRQ.getGetAncillaryOffersRQ().setIsoCurrencyCode("CLP");
      ancOfferRQ.getGetAncillaryOffersRQ().setReservation(new Reservation());
      ancOfferRQ.getGetAncillaryOffersRQ().getReservation().setRecordLocator("AAAAAA");
      ancOfferRQ.getGetAncillaryOffersRQ().setFilter(new Filter());
      ancOfferRQ.getGetAncillaryOffersRQ().setGroupByAncillary("true");
      ancOfferRQ.getGetAncillaryOffersRQ().getFilter().setAncillaryElements(new AncillaryElements());
      AncillaryElement ancillaryElement = new AncillaryElement();
      ancillaryElement.setGroupCode("PT");
      ancOfferRQ.getGetAncillaryOffersRQ().getFilter().getAncillaryElements().getAncillaryElement().add(ancillaryElement);

      return ancOfferRQ;
  }
  
  /**
   * Test method GetAncillaryOffersRQ
   * @return
   */
  public static AncOfferRQ completeFilterAncOfferRQ(String groupByAncillary, String groupCode) {
      AncOfferRQ ancOfferRQ = new AncOfferRQ();
      ancOfferRQ.setGetAncillaryOffersRQ(new com.latam.ancoff.entities.GetAncillaryOffersRQ());
      ancOfferRQ.getGetAncillaryOffersRQ().setAgentPos(new AgentPos());
      ancOfferRQ.getGetAncillaryOffersRQ().getAgentPos().setPcc("EYN");
      ancOfferRQ.getGetAncillaryOffersRQ().setIsoCurrencyCode("CLP");
      ancOfferRQ.getGetAncillaryOffersRQ().setReservation(new Reservation());
      ancOfferRQ.getGetAncillaryOffersRQ().getReservation().setRecordLocator("ZNATOG");
      ancOfferRQ.getGetAncillaryOffersRQ().setFilter(new Filter());
      ancOfferRQ.getGetAncillaryOffersRQ().setGroupByAncillary(groupByAncillary);
      ancOfferRQ.getGetAncillaryOffersRQ().getFilter().setAncillaryElements(new AncillaryElements());
      AncillaryElement ancillaryElement = new AncillaryElement();
      ancillaryElement.setGroupCode(groupCode);
      ancOfferRQ.getGetAncillaryOffersRQ().getFilter().getAncillaryElements().getAncillaryElement().add(ancillaryElement);
      
      return ancOfferRQ;
  }
  
  /**
   * Test method GetAncillaryOffersRQ
   * @return
   */
  public static AncOfferRQ completeAncOfferRQPnrIncompleto() {
      AncOfferRQ ancOfferRQ = new AncOfferRQ();
      ancOfferRQ.setGetAncillaryOffersRQ(new com.latam.ancoff.entities.GetAncillaryOffersRQ());
      ancOfferRQ.getGetAncillaryOffersRQ().setAgentPos(new AgentPos());
      ancOfferRQ.getGetAncillaryOffersRQ().getAgentPos().setPcc("EYN");
      ancOfferRQ.getGetAncillaryOffersRQ().setIsoCurrencyCode("CLP");
      ancOfferRQ.getGetAncillaryOffersRQ().setReservation(new Reservation());
      ancOfferRQ.getGetAncillaryOffersRQ().getReservation().setRecordLocator("AAA");
      return ancOfferRQ;
  }
  
  /**
   * Test method GetAncillaryOffersRQ
   * @return
   */
  public static AncOfferRQ completeAncOfferRQPnrInCorrecto() {
      AncOfferRQ ancOfferRQ = new AncOfferRQ();
      ancOfferRQ.setGetAncillaryOffersRQ(new com.latam.ancoff.entities.GetAncillaryOffersRQ());
      ancOfferRQ.getGetAncillaryOffersRQ().setAgentPos(new AgentPos());
      ancOfferRQ.getGetAncillaryOffersRQ().getAgentPos().setPcc("EYN");
      ancOfferRQ.getGetAncillaryOffersRQ().setIsoCurrencyCode("CLP");
      ancOfferRQ.getGetAncillaryOffersRQ().setReservation(new Reservation());
      ancOfferRQ.getGetAncillaryOffersRQ().getReservation().setRecordLocator("AAAAAA");

      return ancOfferRQ;
  }
  
  @Test
  public void testParameters(){
    Assert.assertNotNull(ConstantsTest.MUST_BE_A_NOTNULL, ancOfferRQ);
  }
  

}
