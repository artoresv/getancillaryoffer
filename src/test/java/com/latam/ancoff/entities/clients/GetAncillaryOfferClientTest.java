package com.latam.ancoff.entities.clients;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.withSettings;

import java.net.MalformedURLException;
import java.net.URL;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.context.ContextConfiguration;

import com.latam.ancoff.clients.GetAncillaryOfferClient;
import com.latam.ancoff.entities.AncOfferRQ;
import com.latam.ancoff.entities.AncOfferRS;
import com.latam.ancoff.entities.GetAncillaryOffersRQTest;
import com.latam.ancoff.entities.GetAncillaryOffersRSTest;
import com.latam.ancoff.entities.AncOfferRQTest;
import com.latam.ancoff.entities.GetReservationResponseTest;
import com.latam.ancoff.utils.AppProperties;
import com.latam.host.sabre.ws.WSSabreSessionManager;
import com.latam.pax.ancillary.offer.From;
import com.latam.pax.ancillary.offer.GetAncillaryOffersFault;
import com.latam.pax.ancillary.offer.GetAncillaryOffersPortType;
import com.latam.pax.ancillary.offer.GetAncillaryOffersRQ;
import com.latam.pax.ancillary.offer.To;
import com.latam.pax.getreservation.PassengerTypePNRB;

import lombok.extern.slf4j.Slf4j;

@RunWith(MockitoJUnitRunner.class)
@ContextConfiguration
@Slf4j
public class GetAncillaryOfferClientTest {

    private static final String WSDL = "http://files.developer.sabre.com/wsdl/sabreXML1.0.00/Merchandising/GAO3/GetAncillaryOfferService_v3_02.wsdl";
    private static final String MENSAJE = "prueba";
    private static final String FAREBASIS_RS = "QLESLH6B";
    boolean result = true;

    GetAncillaryOfferClient getAncillaryOfferClient;
    AppProperties appProperties = new AppProperties();

    WSSabreSessionManager wsSession;
    URL url;

    @Resource
    GetAncillaryOffersPortType getAncillaryOffersPortType;

    @Before
    public void beforeData() {
        appProperties.setGetAncillaryOfferWSDL(WSDL);
        try {
            url = new URL(WSDL);
        } catch (MalformedURLException e) {
            log.info("Error MalformedURLException : ", e);
        }
        getAncillaryOfferClient = new GetAncillaryOfferClient(appProperties);
        wsSession = mock(WSSabreSessionManager.class);
        when(wsSession.getIPCC()).thenReturn("IPCC");
        when(wsSession.getConversationId()).thenReturn("ConversationId");
        Mockito.lenient().when(wsSession.getEndPoint()).thenReturn(WSDL);

        try {
            getAncillaryOffersPortType = Mockito.mock(GetAncillaryOffersPortType.class, withSettings().lenient());
            Mockito.lenient()
                    .when(getAncillaryOffersPortType.getAncillaryOffers(Mockito.any(), Mockito.any(), Mockito.any()))
                    .thenReturn(GetAncillaryOffersRSTest.completeGetAncillaryOffersRS());
        } catch (GetAncillaryOffersFault e) {
            log.info("Error getAncillaryOffers : ", e);
        }
    }

    @Test
    public void testGetMessageData() {
        assertNotNull(MENSAJE, getAncillaryOfferClient.getMessageData());
    }

    @Test
    public void testGetMessageHeader() {
        assertNotNull(MENSAJE, getAncillaryOfferClient.getMessageHeader(wsSession, new From(), new To(),
                getAncillaryOfferClient.getMessageData()));
    }

    @Test
    public void testGetService() {
        assertNotNull(MENSAJE, getAncillaryOfferClient.getService());
    }

    @Test
    public void testGetInstanceService() {
        assertNotNull(MENSAJE, getAncillaryOfferClient.getInstanceService(url));
        assertNotNull(MENSAJE, getAncillaryOfferClient.getInstanceService(null));
    }

    @Test
    public void testGetAncillaryClient() {
        GetAncillaryOffersRQ getAncillaryOffersRQ = GetAncillaryOffersRQTest.completeGetAncillaryOffersRQ();
        AncOfferRS ancOfferRS = new AncOfferRS();
        AncOfferRQ ancOfferRQ = new AncOfferRQ();
        AncOfferRQ ancFilterOfferRQ = AncOfferRQTest.completeFilterAncOfferRQ("true","PT");

        try {
            getAncillaryOfferClient.execute(wsSession,
                    GetReservationResponseTest.completeGetReservationResponse(null, PassengerTypePNRB.S, true, FAREBASIS_RS, false),
                    getAncillaryOffersRQ, ancOfferRQ, ancOfferRS);
            when(wsSession.getBinarySecurityToken()).thenReturn("");
            getAncillaryOfferClient.execute(wsSession,
                    GetReservationResponseTest.completeGetReservationResponse(null, PassengerTypePNRB.S, true, FAREBASIS_RS, false),
                    getAncillaryOffersRQ, ancFilterOfferRQ, ancOfferRS);
            when(wsSession.getBinarySecurityToken()).thenReturn("1234567890");
            getAncillaryOfferClient.execute(wsSession,
                    GetReservationResponseTest.completeGetReservationResponse(null, PassengerTypePNRB.S, true, FAREBASIS_RS, false),
                    getAncillaryOffersRQ, ancOfferRQ, ancOfferRS);
            appProperties.setGetAncillaryOfferWSDL(null);
            getAncillaryOfferClient.execute(wsSession,
                    GetReservationResponseTest.completeGetReservationResponse(null, PassengerTypePNRB.S, true, FAREBASIS_RS, false),
                    getAncillaryOffersRQ, ancOfferRQ, ancOfferRS);
            assertTrue(MENSAJE, result);

        } catch (Exception e) {
            log.info("Error testGetAncillaryClient : ", e);
        }
    }
    

}
