package com.latam.ancoff.entities.clients;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.withSettings;

import java.net.MalformedURLException;
import java.net.URL;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.context.ContextConfiguration;

import com.latam.ancoff.clients.GetReservationClient;
import com.latam.ancoff.entities.AncOfferRS;
import com.latam.ancoff.utils.AppProperties;
import com.latam.host.sabre.ws.WSSabreSessionManager;
import com.latam.pax.getreservation.From;
import com.latam.pax.getreservation.GetReservationPortType;
import com.latam.pax.getreservation.GetReservationRS;
import com.latam.pax.getreservation.MessageData;
import com.latam.pax.getreservation.To;

import lombok.extern.slf4j.Slf4j;

@RunWith(MockitoJUnitRunner.class)
@ContextConfiguration
@Slf4j
public class GetReservationClientTest {

    private static final String WSDL = "http://webservices.sabre.com/wsdl/sabreXML1.0.00/pnrservices/GetReservation_1.19.0.wsdl";
    private static final String RECORDLOCATOR = "XXXXXX";
    private static final String MENSAJE = "mensaje de prueba";
    boolean result = true;
    
    GetReservationClient getReservationClient;
    AppProperties appProperties = new AppProperties();

    WSSabreSessionManager wsSession;
    URL url;

    @Resource
    GetReservationPortType getReservationPortType;

    @Before
    public void beforeData() {
        appProperties.setGetReservationWSDL(WSDL);
        try {
            url = new URL(WSDL);
        } catch (MalformedURLException e) {
            log.info("Error MalformedURLException : ", e);
        }
        getReservationClient = new GetReservationClient(appProperties);
        wsSession = mock(WSSabreSessionManager.class);
        when(wsSession.getIPCC()).thenReturn("IPCC");
        when(wsSession.getConversationId()).thenReturn("ConversationId");
        when(wsSession.getEndPoint()).thenReturn(WSDL);

        getReservationPortType = Mockito.mock(GetReservationPortType.class, withSettings().lenient());
        Mockito.lenient()
                .when(getReservationPortType.getReservationOperation(Mockito.any(), Mockito.any(), Mockito.any()))
                .thenReturn(new GetReservationRS());

    }

    @Test
    public void testGetMessageHeader() {
        assertNotNull(MENSAJE, getReservationClient.getMessageHeader(wsSession, new From(), new To(),
                new MessageData()));
    }

    @Test
    public void testGetService() {
        assertNotNull(MENSAJE, getReservationClient.getService());
    }

    @Test
    public void testGetInstanceService() {
        assertNotNull(MENSAJE, getReservationClient.getInstanceService(url));
        assertNotNull(MENSAJE, getReservationClient.getInstanceService(null));
    }

    @Test
    public void testGetReservationClient() {
        AncOfferRS ancOfferRS = new AncOfferRS();

        try {
            getReservationClient.execute(wsSession,
                    RECORDLOCATOR, ancOfferRS);
            when(wsSession.getBinarySecurityToken()).thenReturn("");
            getReservationClient.execute(wsSession,
                    RECORDLOCATOR, ancOfferRS);
            when(wsSession.getBinarySecurityToken()).thenReturn("1234567890");
            getReservationClient.execute(wsSession,
                    RECORDLOCATOR, ancOfferRS);
            appProperties.setGetReservationWSDL(null);
            getReservationClient.execute(wsSession,
                    RECORDLOCATOR, ancOfferRS);
            assertTrue(MENSAJE, result);

        } catch (Exception e) {
            log.info("Error testGetReservationClient : ", e);
        }
    }

}
