package com.latam.ancoff.entities;


import java.util.ArrayList;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import com.latam.ancoff.entities.utils.ConstantsTest;
import com.latam.pax.getreservation.Airport;
import com.latam.pax.getreservation.FareBreakdown;
import com.latam.pax.getreservation.FareBreakdown.FareComponent;
import com.latam.pax.getreservation.FareBreakdown.FareComponent.FlightSegmentNumbers;
import com.latam.pax.getreservation.FareBreakdown.FlightSegment;
import com.latam.pax.getreservation.FrequentFlyer;
import com.latam.pax.getreservation.FrequentFlyerPNRB;
import com.latam.pax.getreservation.GetReservationRS;
import com.latam.pax.getreservation.Loyalty;
import com.latam.pax.getreservation.OpenReservationElementType;
import com.latam.pax.getreservation.OpenReservationElementsType;
import com.latam.pax.getreservation.PassengerPNRB;
import com.latam.pax.getreservation.PassengerTypePNRB;
import com.latam.pax.getreservation.PricedItineraryPNRB;
import com.latam.pax.getreservation.PricedItineraryPNRB.AirItineraryPricingInfo;
import com.latam.pax.getreservation.ReservationPNRB;
import com.latam.pax.getreservation.SegmentTypePNRB;

@RunWith(MockitoJUnitRunner.class)
public class GetReservationResponseTest {
    
  private static final Integer SEGMENT_NUMBER = 201; 
  private static final String DEPARTURE_TIME = "2019-03-19T15:05:00";
  private static final String FREQUENT_FLYER_NUMBER = "107619941";

  private static GetReservationResponse getReservationResponse = completeGetReservationResponse(null, PassengerTypePNRB.S, true, "QLESLH6B", false);
  
  /**
   * Test method GetReservationResponse
   * @return
   */
  public static GetReservationResponse completeGetReservationResponse(String passengerType, PassengerTypePNRB nameType, boolean frequentFlyer, String fareBasis, boolean isNoFare) {

      GetReservationResponse reservationRS = new GetReservationResponse();
      mapperPassenger(reservationRS, passengerType,
              nameType, frequentFlyer);
      mapperPricedItineraryPNRB(reservationRS, fareBasis, isNoFare);       

      mapperSegment(reservationRS);
      OpenReservationElementType openReservationElementType = mapperOpenReservationElementTypes(reservationRS);
      
      GetReservationRS getReservationRS = new GetReservationRS();
      ReservationPNRB reservationPnrb = new ReservationPNRB();
     
      
      reservationPnrb.setNumberInSegment(SEGMENT_NUMBER);
      
      OpenReservationElementsType openReservationElementsType = new OpenReservationElementsType();
      openReservationElementsType.getOpenReservationElement().add(openReservationElementType);
      
      getReservationRS.setReservation(reservationPnrb);
      getReservationRS.getReservation().setOpenReservationElements(openReservationElementsType);
      
      return reservationRS;

  }
  
  private static void mapperPricedItineraryPNRB(GetReservationResponse reservationRS, String fareBasis, boolean isNoFare) {
      
      PricedItineraryPNRB pricedItineraryPNRB = new PricedItineraryPNRB();
      pricedItineraryPNRB.setStatusCode("A");
      pricedItineraryPNRB.setAirItineraryPricingInfo(new AirItineraryPricingInfo());
      FareBreakdown fareBreakdown = new FareBreakdown();
      
      if(!isNoFare) {
          FareComponent fareComponent = new FareComponent();
          fareComponent.setFlightSegmentNumbers(new FlightSegmentNumbers());
          fareComponent.getFlightSegmentNumbers().getFlightSegmentNumber().add("1");
          fareComponent.setFareBasisCode(fareBasis);
          fareBreakdown.getFareComponent().add(fareComponent);
      } else {
          fareBreakdown.getFareBasisCode().add(fareBasis);
          FlightSegment flightSegment = new FlightSegment();
          flightSegment.setDepartureDateTime(DEPARTURE_TIME);
          flightSegment.setFlightNumber("260");
          flightSegment.setAirPort(new Airport());
          flightSegment.getAirPort().setValue("CCP");
          flightSegment.setFareBasisCode(fareBasis);
          fareBreakdown.getFlightSegment().add(flightSegment);
      }
      
      pricedItineraryPNRB.getAirItineraryPricingInfo().getPTCFareBreakdown().add(fareBreakdown);
      reservationRS.setPricedItinerary(new ArrayList<PricedItineraryPNRB>());
      reservationRS.getPricedItinerary().add(pricedItineraryPNRB);
      
  }
  
    private static void mapperPassenger(GetReservationResponse reservationRS, String passengerType,
            PassengerTypePNRB nameType, boolean frequentFlyer) {
        reservationRS.setPassengers(new ArrayList<PassengerPNRB>());
        PassengerPNRB passengerPNRB = new PassengerPNRB();
        passengerPNRB.setNameId("01.01");
        passengerPNRB.setFirstName("FIRSTNAME");
        passengerPNRB.setLastName("LASTNAME");
        passengerPNRB.setNameType(nameType);
        passengerPNRB.setPassengerType(passengerType);
        infoFrequentFlyer(passengerPNRB, frequentFlyer);
        reservationRS.getPassengers().add(passengerPNRB);
    }
    
    private static void mapperSegment(GetReservationResponse reservationRS) {
        reservationRS.setSegment(new ArrayList<SegmentTypePNRB.Segment>());
        SegmentTypePNRB.Segment segment = new SegmentTypePNRB.Segment();
        segment.setSequence(new Short("1"));
        segment.setAir(new SegmentTypePNRB.Segment.Air());
        segment.getAir().setArrivalAirport("SCL");
        segment.getAir().setDepartureAirport("CCP");
        segment.getAir().setArrivalDateTime("2019-03-20T15:05:00");
        segment.getAir().setClassOfService("Q");
        segment.getAir().setDepartureDateTime(DEPARTURE_TIME);
        segment.getAir().setFlightNumber("260");
        segment.getAir().setOperatingAirlineCode("LA");
        reservationRS.getSegment().add(segment);
    }
    
    private static OpenReservationElementType mapperOpenReservationElementTypes(GetReservationResponse reservationRS) {
        reservationRS.setOpenReservationElementTypes(new ArrayList<OpenReservationElementType>());
        OpenReservationElementType openReservationElementType = new OpenReservationElementType();
        openReservationElementType.setLoyalty(new Loyalty());
        openReservationElementType.getLoyalty().setFrequentFlyer(new FrequentFlyer());
        openReservationElementType.getLoyalty().getFrequentFlyer().setReceivingCarrierCode("LA");
        openReservationElementType.getLoyalty().getFrequentFlyer().setFrequentFlyerNumber(FREQUENT_FLYER_NUMBER);
        openReservationElementType.getLoyalty().getFrequentFlyer().setBanner("BLACK");
        openReservationElementType.getLoyalty().getFrequentFlyer().setTag("BLK/EMD");
        openReservationElementType.getLoyalty().getFrequentFlyer().setVitType(new Short("4"));
        reservationRS.getOpenReservationElementTypes().add(openReservationElementType);
        return openReservationElementType;
    }
  
    private static void infoFrequentFlyer(PassengerPNRB passengerPNRB, boolean frequentFlyer) {
        if (frequentFlyer) {
            FrequentFlyerPNRB frequentFlyerPNRB = new FrequentFlyerPNRB();
            frequentFlyerPNRB.setNumber(FREQUENT_FLYER_NUMBER);
            passengerPNRB.getFrequentFlyer().add(frequentFlyerPNRB);
        }
    }

    @Test
    public void testParameters() {
        Assert.assertNotNull(ConstantsTest.MUST_BE_A_NOTNULL, getReservationResponse);
    }
  

}
