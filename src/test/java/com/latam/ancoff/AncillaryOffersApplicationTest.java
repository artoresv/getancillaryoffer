package com.latam.ancoff;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.latam.ancoff.controller.MainController;
import com.latam.ancoff.entities.AgentPos;
import com.latam.ancoff.entities.AncOfferRQ;
import com.latam.ancoff.entities.GetAncillaryOffersRQ;
import com.latam.ancoff.entities.Reservation;
import com.latam.ancoff.services.AncillaryOfferService;

import lombok.extern.slf4j.Slf4j;

@RunWith(SpringRunner.class)
@SpringBootTest
@WebAppConfiguration
@Slf4j
public class AncillaryOffersApplicationTest {

    @Autowired
    MainController main = new MainController();

    @Autowired
    AncillaryOfferService client;
    
    @Autowired MockHttpSession session;
    
    @Autowired MockHttpServletRequest request;
    
    @Autowired WebApplicationContext wac; 

    AncOfferRQ rq = new AncOfferRQ();
    private MockMvc mockMvc;
    
    ObjectMapper mapper = new ObjectMapper();
    
    private static String urlApi = "/commercial/inventory/reservationsandticketing/reservation/v1/ancillaryoffers";
    private static String headersName = "LAN-ApplicationName";
    private static String headersValue = "Payfields";

    private static String headersSsidName = "LAN-SSID";
    private static String headersSsidValue = "990041-0:0:0:0:0:0:0:1";
    
    @Before
    public void fillData() {
        rq.setGetAncillaryOffersRQ(new GetAncillaryOffersRQ());
        rq.getGetAncillaryOffersRQ().setReservation(new Reservation());
        rq.getGetAncillaryOffersRQ().setAgentPos(new AgentPos());
        
        rq.getGetAncillaryOffersRQ().getReservation().setRecordLocator("ZNATOG");
        rq.getGetAncillaryOffersRQ().setIsoCurrencyCode("CLP");
        rq.getGetAncillaryOffersRQ().getAgentPos().setPcc("EYN");
        
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
    }

    @Test
    public void testProff(){

        mapper.enable(SerializationFeature.INDENT_OUTPUT);
        
        try {
            mockMvc.perform(MockMvcRequestBuilders
                    .post(urlApi)
                    .content( mapper.writeValueAsString(rq) )
                    .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                    .header(headersName, headersValue))
                    .andExpect(MockMvcResultMatchers.status().isOk());
        } catch (Exception e) {
            log.info("Error testProff : ", e);
        }


    }
    
    
    @Test
    public void testProff2(){

        mapper.enable(SerializationFeature.INDENT_OUTPUT);
        
        rq.getGetAncillaryOffersRQ().getReservation().setRecordLocator("PRQUFK");
        rq.getGetAncillaryOffersRQ().setIsoCurrencyCode("CLP");
        rq.getGetAncillaryOffersRQ().getAgentPos().setPcc("EYN");
        rq.getGetAncillaryOffersRQ().setGroupByAncillary("true");
        
        try {
            mockMvc.perform(MockMvcRequestBuilders
                    .post(urlApi)
                    .content( mapper.writeValueAsString(rq) )
                    .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                    .header(headersName, headersValue))
                    .andExpect(MockMvcResultMatchers.status().isOk());
        } catch (Exception e) {
            log.info("Error testProff2 : ", e);
        }


    }
    
    @Test
    public void testPnrBlank(){
        
        mapper.enable(SerializationFeature.INDENT_OUTPUT);
        
        rq.getGetAncillaryOffersRQ().getReservation().setRecordLocator("");
        rq.getGetAncillaryOffersRQ().setIsoCurrencyCode("CLP");
        rq.getGetAncillaryOffersRQ().getAgentPos().setPcc("EYN");
        
        try {
            mockMvc.perform(MockMvcRequestBuilders
                    .post(urlApi)
                    .content( mapper.writeValueAsString(rq) )
                    .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                    .header(headersName, headersValue))
                    .andExpect(MockMvcResultMatchers.status().isNotFound());
        } catch (Exception e) {
            log.info("Error testPnrBlank : ", e);
        }
    }
    
    @Test
    public void testPnrNotFound(){
        
        mapper.enable(SerializationFeature.INDENT_OUTPUT);
        
        rq.getGetAncillaryOffersRQ().getReservation().setRecordLocator("GWINCI");
        rq.getGetAncillaryOffersRQ().setIsoCurrencyCode("CLP");
        rq.getGetAncillaryOffersRQ().getAgentPos().setPcc("EYN");
        
        try {
            mockMvc.perform(MockMvcRequestBuilders
                    .post(urlApi)
                    .content( mapper.writeValueAsString(rq) )
                    .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                    .header(headersSsidName, headersSsidValue)
                    .header(headersName, headersValue))
                    .andExpect(MockMvcResultMatchers.status().isOk());
        } catch (Exception e) {
            log.info("Error testPnrNotFound : ", e);
        }
    }
    
    @Test
    public void testPnrVacio() {
        mapper.enable(SerializationFeature.INDENT_OUTPUT);
        
        rq.getGetAncillaryOffersRQ().getReservation().setRecordLocator("");
        rq.getGetAncillaryOffersRQ().setIsoCurrencyCode("");
        rq.getGetAncillaryOffersRQ().getAgentPos().setPcc("");
        
        
            try {
                mockMvc.perform(MockMvcRequestBuilders
                        .post(urlApi)
                        .content( mapper.writeValueAsString(rq) )
                        .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                        .header(headersName, headersValue))
                        .andExpect(MockMvcResultMatchers.status().isNotFound());
            } catch (Exception e) {
                log.info("Error testPnrVacio : ", e);
            }

    }
    
    @Test
    public void testHeadersVacio() {
        mapper.enable(SerializationFeature.INDENT_OUTPUT);
        
        rq.getGetAncillaryOffersRQ().getReservation().setRecordLocator("");
        rq.getGetAncillaryOffersRQ().setIsoCurrencyCode("");
        rq.getGetAncillaryOffersRQ().getAgentPos().setPcc("");
        
        
            try {
                mockMvc.perform(MockMvcRequestBuilders
                        .post(urlApi)
                        .content( mapper.writeValueAsString(rq) )
                        .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                        .header(headersName, ""))
                        .andExpect(MockMvcResultMatchers.status().isNotFound());
            } catch (Exception e) {
                log.info("Error testHeadersVacio : ", e);
            }

    }
    
    @Test
    public void testRQVacio() {
        mapper.enable(SerializationFeature.INDENT_OUTPUT);
        
        rq.setGetAncillaryOffersRQ(null);
        
        
            try {
                mockMvc.perform(MockMvcRequestBuilders
                        .post(urlApi)
                        .content( mapper.writeValueAsString(rq) )
                        .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                        .header(headersName, headersValue))
                        .andExpect(MockMvcResultMatchers.status().isNotFound());
            } catch (Exception e) {
                log.info("Error testRQVacio : ", e);
            }

    }
    
    @Test
    public void testReservationVacio() {
        mapper.enable(SerializationFeature.INDENT_OUTPUT);
        
        rq.getGetAncillaryOffersRQ().setReservation(null);
        
            try {
                mockMvc.perform(MockMvcRequestBuilders
                        .post(urlApi)
                        .content( mapper.writeValueAsString(rq) )
                        .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                        .header(headersName, headersValue))
                        .andExpect(MockMvcResultMatchers.status().isNotFound());
            } catch (Exception e) {
                log.info("Error testReservationVacio : ", e);
            }

    }
    
    @Test
    public void testPCCVacio() {
        mapper.enable(SerializationFeature.INDENT_OUTPUT);
        
        rq.getGetAncillaryOffersRQ().getAgentPos().setPcc(null);
        
            try {
                mockMvc.perform(MockMvcRequestBuilders
                        .post(urlApi)
                        .content( mapper.writeValueAsString(rq) )
                        .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                        .header(headersName, headersValue))
                        .andExpect(MockMvcResultMatchers.status().isNotFound());
            } catch (Exception e) {
                log.info("Error testPCCVacio : ", e);
            }

    }
    
    @Test
    public void testAgentePosVacio() {
        mapper.enable(SerializationFeature.INDENT_OUTPUT);
        
        rq.getGetAncillaryOffersRQ().setAgentPos(null);
        
            try {
                mockMvc.perform(MockMvcRequestBuilders
                        .post(urlApi)
                        .content( mapper.writeValueAsString(rq) )
                        .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                        .header(headersName, headersValue))
                        .andExpect(MockMvcResultMatchers.status().isNotFound());
            } catch (Exception e) {
                log.info("Error testAgentePosVacio : ", e);
            }

    }
    
    @Test
    public void testISOCurrencyVacio() {
        mapper.enable(SerializationFeature.INDENT_OUTPUT);
        
        rq.getGetAncillaryOffersRQ().setIsoCurrencyCode(null);
        
            try {
                mockMvc.perform(MockMvcRequestBuilders
                        .post(urlApi)
                        .content( mapper.writeValueAsString(rq) )
                        .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                        .header(headersName, headersValue))
                        .andExpect(MockMvcResultMatchers.status().isNotFound());
            } catch (Exception e) {
                log.info("Error testISOCurrencyVacio : ", e);
            }

    }
    


}
