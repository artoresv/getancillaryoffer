package com.latam.ancoff.impl;

import java.util.List;
import java.util.stream.Collectors;

import javax.xml.ws.WebServiceException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.latam.ancoff.clients.GetAncillaryOfferClient;
import com.latam.ancoff.clients.GetReservationClient;
import com.latam.ancoff.entities.AncOfferRQ;
import com.latam.ancoff.entities.AncOfferRS;
import com.latam.ancoff.entities.GetReservationResponse;
import com.latam.ancoff.exception.AncillaryOfferException;
import com.latam.ancoff.services.AncillaryOfferService;
import com.latam.ancoff.utils.AppProperties;
import com.latam.ancoff.utils.Constants;
import com.latam.ancoff.utils.MapperResponseUtil;
import com.latam.ancoff.utils.ValidationUtils;
import com.latam.host.commons.ws.exceptions.WSHostException;
import com.latam.host.sabre.ws.WSSabreSessionManager;
import com.latam.pax.ancillary.offer.GetAncillaryOffersRQ;
import com.latam.pax.ancillary.offer.GetAncillaryOffersRS;
import com.latam.pax.getreservation.GetReservationRS;
import com.latam.pax.getreservation.PricedItineraryPNRB;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class AncillaryOfferImpl implements AncillaryOfferService {

    @Autowired
    private AppProperties property;

    public AncOfferRS process(String appName, String lanSSID, AncOfferRQ request) throws AncillaryOfferException {

        AncOfferRS ancOfferRS = new AncOfferRS();

        WSSabreSessionManager ssm = new WSSabreSessionManager(property.getSessionCreateEndpoint(),
                property.getSessionCloseEndpoint());

        try {

            if (null != lanSSID) {
                ssm.open(appName, WSSabreSessionManager.CLIENT_TYPE_APPLICATION, property.getAirlineCode(), lanSSID,
                        WSSabreSessionManager.SESSIONTYPE_REQUIRED);
            } else {
                ssm.open(appName, WSSabreSessionManager.CLIENT_TYPE_APPLICATION, property.getAirlineCode());
            }

            //Logic Business
            ancillaryLogic(request, ancOfferRS, ssm);

        } catch (WSHostException e) {
            log.error(Constants.ERROR_MESSAGE_TITLE + e.getMessage() + "," + Constants.NATIVE_MESSAGE_TITLE
                    + e.getNativeMessage(), e);

            ValidationUtils.validateException(ancOfferRS, Constants.WS_ERROR_OPEN_SESSION_CODE, e.getMessage(),
                    e.getNativeMessage());

        } catch (WebServiceException e) {
            log.error(Constants.ERROR_MESSAGE_TITLE + e.getMessage(), e);

            ValidationUtils.validateException(ancOfferRS, Constants.WS_ERROR_OPEN_SESSION_CODE, e.getMessage(),
                    e.getMessage());

        } finally {
            try {

                ssm.close();

            } catch (WSHostException e) {
                log.error(Constants.ERROR_MESSAGE_TITLE + e.getMessage() + "," + Constants.NATIVE_MESSAGE_TITLE
                        + e.getNativeMessage(), e);

                ValidationUtils.validateException(ancOfferRS, Constants.WS_ERROR_CLOSE_SESSION_CODE, e.getMessage(),
                        e.getNativeMessage());
            }
        }

        return ancOfferRS;

    }
    
    /**
     * 
     * Method include ancillary logic
     * 
     * @param request
     * @param ancOfferRS
     * @param wsSession
     * @throws AncillaryOfferException
     */
    public void ancillaryLogic(AncOfferRQ request, AncOfferRS ancOfferRS, WSSabreSessionManager wsSession) throws AncillaryOfferException {
        
      if( isErrorService(ancOfferRS) ){
          throw new AncillaryOfferException(
                  ancOfferRS.getGetAncillaryOffersRS().getServiceStatus().getCode(), 
                  ancOfferRS.getGetAncillaryOffersRS().getServiceStatus().getMessage(),
                  ancOfferRS.getGetAncillaryOffersRS().getServiceStatus().getNativeMessage());
      }
      
      
      log.info(wsSession.getBinarySecurityToken());
      log.info("PNR :: " + request.getGetAncillaryOffersRQ().getReservation().getRecordLocator());
      boolean lengthRecordLocator = !ValidationUtils
              .validateStringNotNull(request.getGetAncillaryOffersRQ().getReservation().getRecordLocator())
              && request.getGetAncillaryOffersRQ().getReservation().getRecordLocator()
                      .length() == Constants.LENGTH_PNR;
      if (lengthRecordLocator) {
          GetReservationRS getReservationRS;
          GetReservationClient getReservationClient = new GetReservationClient(property);
          getReservationRS = getReservationClient.execute(wsSession,
                  request.getGetAncillaryOffersRQ().getReservation().getRecordLocator(), ancOfferRS);
          
          if( isErrorService(ancOfferRS) ){
              throw new AncillaryOfferException(
                      ancOfferRS.getGetAncillaryOffersRS().getServiceStatus().getCode(), 
                      ancOfferRS.getGetAncillaryOffersRS().getServiceStatus().getMessage(),
                      ancOfferRS.getGetAncillaryOffersRS().getServiceStatus().getNativeMessage());
          }
          
          if ( null != getReservationRS.getErrors() ) {
              throw new AncillaryOfferException(
                      Integer.parseInt( getReservationRS.getErrors().getError().get(0).getCode() ), 
                      getReservationRS.getErrors().getError().get(0).getMessage(),
                      getReservationRS.getErrors().getError().get(0).getMessage());
          }

          GetReservationResponse reservationRS = reservationSetRS(getReservationRS,
                  request.getGetAncillaryOffersRQ().getIsoCurrencyCode(),
                  request.getGetAncillaryOffersRQ().getAgentPos().getPcc());
          
          boolean isSegmentInfo = null == reservationRS.getSegment() || reservationRS.getSegment().isEmpty();
          if(isSegmentInfo) {
               throw new AncillaryOfferException(Constants.WS_ERROR_SEGMENTS_CODE, 
                       property.getErrorSegmentsStr(), property.getErrorSegmentsStr());
          }
          
          List<PricedItineraryPNRB> listPricedItineraryValidate = reservationRS.getPricedItinerary().stream()
                  .filter(value -> "A".equalsIgnoreCase(value.getStatusCode())).collect(Collectors.toList());
          if(null == listPricedItineraryValidate || listPricedItineraryValidate.isEmpty()) {
              throw new AncillaryOfferException(Constants.WS_ERROR_PQ_CODE, 
                      property.getErrorPqStr(), property.getErrorPqStr());
          }

          GetAncillaryOfferClient getAncillaryOfferClient = new GetAncillaryOfferClient(property);
          GetAncillaryOffersRQ getAncillaryOffersRQ = new GetAncillaryOffersRQ();
          
          GetAncillaryOffersRS getAncillaryOffersRS = getAncillaryOfferClient.execute(wsSession, reservationRS,
                    getAncillaryOffersRQ, request, ancOfferRS);

          MapperResponseUtil.mapperAncOfferRS(request, ancOfferRS, getAncillaryOffersRS, getAncillaryOffersRQ, reservationRS, property);

      } else {
          log.info("Error Pnr vacio o es menor a 6 caracteres");
      }
    }
    
    
    /**
     * 
     * Method validate error service
     * 
     * @param ancOfferRS
     * @return
     */
    private static boolean isErrorService(AncOfferRS ancOfferRS) {
        return null != ancOfferRS.getGetAncillaryOffersRS()
                && null != ancOfferRS.getGetAncillaryOffersRS().getServiceStatus()
                && 0 != ancOfferRS.getGetAncillaryOffersRS().getServiceStatus().getCode();
    }

    public GetReservationResponse reservationSetRS(GetReservationRS getReservationRS, String isoCurrencyCode,
            String pcc) {

        GetReservationResponse reservationRS = new GetReservationResponse();

        reservationRS.setIsoCurrencyCode(isoCurrencyCode);
        reservationRS.setPcc(pcc);

        reservationRS.setTicketDetails(
                getReservationRS.getReservation().getPassengerReservation().getTicketingInfo().getTicketDetails());
        reservationRS.setPassengers(
                getReservationRS.getReservation().getPassengerReservation().getPassengers().getPassenger());
        reservationRS
                .setSegment(getReservationRS.getReservation().getPassengerReservation().getSegments().getSegment());

        reservationRS.setPricedItinerary(
                getReservationRS.getReservation().getPassengerReservation().getItineraryPricing().getPricedItinerary());

        boolean isOpenReservationElement = null != getReservationRS.getReservation()
                && null != getReservationRS.getReservation().getOpenReservationElements()
                && null != getReservationRS.getReservation().getOpenReservationElements().getOpenReservationElement();
        if (isOpenReservationElement) {
            reservationRS.setOpenReservationElementTypes(
                    getReservationRS.getReservation().getOpenReservationElements().getOpenReservationElement());
        }

        return reservationRS;
    }

}
