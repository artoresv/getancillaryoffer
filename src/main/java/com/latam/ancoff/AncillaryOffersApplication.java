package com.latam.ancoff;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan("com.latam")
@SpringBootApplication
public class AncillaryOffersApplication {

    public static void main(String[] args) {
        SpringApplication.run(AncillaryOffersApplication.class, args);
    }

}
