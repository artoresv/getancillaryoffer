package com.latam.ancoff.exception;

import lombok.Getter;

/**
 * The Class AncillaryOfferException.
 *
 * @author everis.com
 */

public class AncillaryOfferException extends Exception {
    private static final long serialVersionUID = 6183592291687362311L;
    @Getter
    private final int code;
    @Getter
    private final String nativeMessage;

    /**
     * Instantiates a new DisplayRewardPayException exception.
     */

    /**
     * Instantiates a new AncillaryOfferException exception.
     * 
     * @param message the message
     */
    public AncillaryOfferException(int code, String message, Throwable e) {
        super(message);
        this.code = code;
        this.nativeMessage = e.toString();
    }

    /**
     * Instantiates a new AncillaryOfferException exception.
     *
     * @param code          the code
     * @param message       the message
     * @param nativeMessage the native message
     */
    public AncillaryOfferException(int code, String message, String nativeMessage) {
        super(message);
        this.code = code;
        this.nativeMessage = nativeMessage;
    }

}
