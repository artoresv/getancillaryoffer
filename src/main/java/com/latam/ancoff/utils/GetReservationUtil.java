package com.latam.ancoff.utils;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

import com.latam.host.sabre.ws.WSSabreSessionManager;
import com.latam.pax.getreservation.MessageData;
import com.latam.pax.getreservation.PartyId;
import com.latam.pax.getreservation.Security;

public final class GetReservationUtil {
    
    private static Locale locale = new Locale(Constants.LOCALE_MIN, Constants.LOCALE_US_MAY);
    
    private GetReservationUtil() {
        
    }

    public static PartyId getReservationGetParty(WSSabreSessionManager wsSession) {
        PartyId toPartyId = new PartyId();
        toPartyId.setType(Constants.MSG_URI);
        toPartyId.setValue(wsSession.getTo());
        return toPartyId;
    }

    public static PartyId getReservationGetPartyId(WSSabreSessionManager wsSession) {
        PartyId fromPartyId = new PartyId();
        fromPartyId.setType(Constants.MSG_URI);
        fromPartyId.setValue(wsSession.getFrom());
        return fromPartyId;
    }

    public static Security getReservationGetSecurity(WSSabreSessionManager wsSession) {
        Security security = new Security();
        security.setBinarySecurityToken(wsSession.getBinarySecurityToken());
        return security;
    }
    
    /**
     * Gets the message data.
     *
     * @return the message data
     */
    public static MessageData getReservationGetMessageData() {
        DateTimeFormatter formatDate = DateTimeFormatter.ofPattern(Constants.CLIENT_DATE_FORMAT, locale);
        DateTimeFormatter formatDateMID = DateTimeFormatter.ofPattern(Constants.CLIENT_DATE_FORMAT_MID, locale);
        MessageData messageData = new MessageData();
        messageData.setRefToMessageId(LocalDateTime.now().format(formatDateMID));
        messageData.setTimestamp(LocalDateTime.now().format(formatDate));
        messageData.setMessageId(LocalDateTime.now().format(formatDateMID));
        return messageData;
    }
    
}
