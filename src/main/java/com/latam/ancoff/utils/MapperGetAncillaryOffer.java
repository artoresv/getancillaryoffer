package com.latam.ancoff.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import com.latam.ancoff.entities.AncOfferRQ;
import com.latam.ancoff.entities.GetReservationResponse;
import com.latam.pax.ancillary.offer.ClientContext;
import com.latam.pax.ancillary.offer.FareInfo;
import com.latam.pax.ancillary.offer.Flight;
import com.latam.pax.ancillary.offer.GetAncillaryOffersRQ;
import com.latam.pax.ancillary.offer.GetAncillaryOffersRQ.QueryByItinerary;
import com.latam.pax.ancillary.offer.RequestType;
import com.latam.pax.ancillary.offer.Segment;
import com.latam.pax.getreservation.FareBreakdown;
import com.latam.pax.getreservation.FareBreakdown.FlightSegment;
import com.latam.pax.getreservation.PricedItineraryPNRB;
import com.latam.pax.getreservation.SegmentTypePNRB;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public final class MapperGetAncillaryOffer extends MapperGetAncillaryOfferBase {

    private MapperGetAncillaryOffer() {

    }

    public static void getRequest(GetAncillaryOffersRQ request, GetReservationResponse reservationRS,
            AncOfferRQ ancOfferRQ, AppProperties property) {
        request.setVersion(property.getAncillaryOfferVersion);
        request.setRequestType(RequestType.PAYLOAD);
        request.setRequestMode("booking");

        /*** ClientContext ***/
        ClientContext clientContext = new ClientContext();
        clientContext.setClientType("");
        clientContext.setCityCode(reservationRS.getPcc());
        clientContext.setPCC(reservationRS.getPcc());
        clientContext.setAirlineCarrierCode(property.airlineCode);
        clientContext.setAgentCurrencyCode(reservationRS.getIsoCurrencyCode());
        request.setClientContext(clientContext);
        
        mapperAnillaryOptions(request, ancOfferRQ, property);

        /*** QueryByItinerary ***/
        QueryByItinerary queryByItinerary = new QueryByItinerary();

        List<PricedItineraryPNRB> listPricedItinerary = reservationRS.getPricedItinerary().stream()
                .filter(value -> "A".equalsIgnoreCase(value.getStatusCode())).collect(Collectors.toList());

        List<String> listFlightSegmentNumber = new ArrayList<>();
        Map<String, List<FareInfo>> mapFareInfo = new HashMap<>();
        int count = 0;
        for (PricedItineraryPNRB pricedItineraryPNRB : listPricedItinerary) {
            for (FareBreakdown fareBreakdown : pricedItineraryPNRB.getAirItineraryPricingInfo().getPTCFareBreakdown()) {
                for (FareBreakdown.FareComponent fareComponent : fareBreakdown.getFareComponent()) {

                    listFlightSegmentNumber.addAll(fareComponent.getFlightSegmentNumbers().getFlightSegmentNumber());
                    listFlightSegmentNumber.stream().distinct().collect(Collectors.toList());

                    count = mapperFare(fareComponent, count, queryByItinerary, mapFareInfo);

                }
            }
        }
        
        if(count == 0) {
            log.info("There is no Fare, the segments are traversed");
            validateWithoutNoFare(count, listPricedItinerary, 
                    reservationRS,
                    listFlightSegmentNumber,
                    queryByItinerary,
                    mapFareInfo);
        }

        List<SegmentTypePNRB.Segment> listSegment = reservationRS.getSegment().stream()
                .filter(value -> listFlightSegmentNumber.contains(value.getSequence().toString()))
                .collect(Collectors.toList());

        Map<String, Segment> mapSegment = new HashMap<>();
        mapperSegment(listSegment, mapSegment);

        for (Map.Entry<String, Segment> entry : mapSegment.entrySet()) {
            queryByItinerary.getSegment().add(entry.getValue());
        }

        GetAncillaryOfferUtil.mapperPassenger(ancOfferRQ, reservationRS, mapSegment, mapFareInfo, queryByItinerary);

        request.setQueryByItinerary(queryByItinerary);

    }

    public static void mapperSegment(List<SegmentTypePNRB.Segment> listSegment, Map<String, Segment> mapSegment) {
        for (SegmentTypePNRB.Segment segmentAux : listSegment) {
            Segment segment = new Segment();
            segment.setId(Constants.SEGMENT + segmentAux.getSequence());
            segment.setSegmentNumber(String.valueOf(segmentAux.getSequence()));

            Flight flight = new Flight();
            flight.setId(Constants.FLIGHT + segmentAux.getSequence());
            flight.setAirline(segmentAux.getAir().getOperatingAirlineCode());
            flight.setFlightNumber(segmentAux.getAir().getFlightNumber());
            flight.setDepartureAirport(segmentAux.getAir().getDepartureAirport());
            flight.setArrivalAirport(segmentAux.getAir().getArrivalAirport());

            XMLGregorianCalendar departureDate = null;
            XMLGregorianCalendar arrivalDate = null;
            XMLGregorianCalendar departureTime = null;
            XMLGregorianCalendar arrivalTime = null;
            try {
                departureDate = DatatypeFactory.newInstance()
                        .newXMLGregorianCalendar(segmentAux.getAir().getDepartureDateTime());
                departureTime = DatatypeFactory.newInstance()
                        .newXMLGregorianCalendar(segmentAux.getAir().getDepartureDateTime());

                flight.setDepartureDate(departureDate);
                flight.setDepartureTime(departureTime);
                arrivalDate = DatatypeFactory.newInstance()
                        .newXMLGregorianCalendar(segmentAux.getAir().getArrivalDateTime());
                arrivalTime = DatatypeFactory.newInstance()
                        .newXMLGregorianCalendar(segmentAux.getAir().getArrivalDateTime());

                flight.setArrivalDate(arrivalDate);
                flight.setArrivalTime(arrivalTime);
            } catch (DatatypeConfigurationException e) {
                log.error("DatatypeConfigurationException : ", e);
            }

            flight.setOperatingAirline(segmentAux.getAir().getOperatingAirlineCode());
            flight.setOperatingFlightNumber(segmentAux.getAir().getOperatingFlightNumber());
            flight.setClassOfService(segmentAux.getAir().getClassOfService());
            segment.setFlightDetail(flight);

            segment.setInboundConnection(segmentAux.getAir().isInboundConnection());
            segment.setOutboundConnection(segmentAux.getAir().isOutboundConnection());

            mapSegment.put(segmentAux.getSequence().toString(), segment);
        }
    }

    public static int mapperFare(FareBreakdown.FareComponent fareComponent, int count,
            QueryByItinerary queryByItinerary, Map<String, List<FareInfo>> mapFareInfo) {
        for (String id : fareComponent.getFlightSegmentNumbers().getFlightSegmentNumber()) {
            count = count + 1;
            FareInfo fareInfo = new FareInfo();
            fareInfo.setId(Constants.FARE_ID + count);

            if (fareComponent.getFareBasisCode().length() > Constants.LENGTH_FAREBASIS) {
                fareComponent
                        .setFareBasisCode(fareComponent.getFareBasisCode().substring(0, Constants.LENGTH_FAREBASIS));
            }

            fareInfo.setFareBasisCode(fareComponent.getFareBasisCode());
            queryByItinerary.getFareInfo().add(fareInfo);

            List<FareInfo> list = new ArrayList<>();
            list.add(fareInfo);
            if (mapFareInfo.containsKey(id)) {
                mapFareInfo.get(id).addAll(list);
            } else {
                mapFareInfo.put(id, list);
            }

        }

        return count;
    }
    
    public static int mapperWithoutFare(int count, String fareBasisCode, String sequence,
            QueryByItinerary queryByItinerary, Map<String, List<FareInfo>> mapFareInfo) {
        count = count + 1;
        FareInfo fareInfo = new FareInfo();
        fareInfo.setId(Constants.FARE_ID + count);

        if (fareBasisCode.length() > Constants.LENGTH_FAREBASIS) {
            fareBasisCode = fareBasisCode.substring(0, Constants.LENGTH_FAREBASIS);
        }

        fareInfo.setFareBasisCode(fareBasisCode);
        queryByItinerary.getFareInfo().add(fareInfo);

        List<FareInfo> list = new ArrayList<>();
        list.add(fareInfo);
        if (mapFareInfo.containsKey(sequence)) {
            mapFareInfo.get(sequence).addAll(list);
        } else {
            mapFareInfo.put(sequence, list);
        }

        return count;
    }
    
    private static void validateWithoutNoFare(int count, List<PricedItineraryPNRB> listPricedItinerary,
            GetReservationResponse reservationRS, List<String> listFlightSegmentNumber,
            QueryByItinerary queryByItinerary, Map<String, List<FareInfo>> mapFareInfo) {

        log.info("There is no Fare, the segments are traversed");
        for (PricedItineraryPNRB pricedItineraryPNRB : listPricedItinerary) {
            for (FareBreakdown fareBreakdown : pricedItineraryPNRB.getAirItineraryPricingInfo().getPTCFareBreakdown()) {
                for (FlightSegment flightSegment : fareBreakdown.getFlightSegment()) {
                    count = mapperSegmentsFare(count, reservationRS, flightSegment, listFlightSegmentNumber,
                            queryByItinerary, mapFareInfo);
                }
            }
        }

    }
    
    private static int mapperSegmentsFare(int count, GetReservationResponse reservationRS, FlightSegment flightSegment,
            List<String> listFlightSegmentNumber,
            QueryByItinerary queryByItinerary,
            Map<String, List<FareInfo>> mapFareInfo) {
        if (null != flightSegment.getFareBasisCode()) {
            List<SegmentTypePNRB.Segment> listValidSegment = reservationRS.getSegment().stream()
                    .filter(value -> value.getAir().getFlightNumber().contains(flightSegment.getFlightNumber())
                            && value.getAir().getDepartureAirport()
                                    .equalsIgnoreCase(flightSegment.getAirPort().getValue())
                            && value.getAir().getDepartureDateTime()
                                    .equalsIgnoreCase(flightSegment.getDepartureDateTime()))
                    .collect(Collectors.toList());
            for (SegmentTypePNRB.Segment segment : listValidSegment) {
                listFlightSegmentNumber.add(segment.getSequence().toString());
                count = mapperWithoutFare(count, flightSegment.getFareBasisCode(), segment.getSequence().toString(),
                        queryByItinerary, mapFareInfo);
            }
        }
        return count;
    }
    

}
