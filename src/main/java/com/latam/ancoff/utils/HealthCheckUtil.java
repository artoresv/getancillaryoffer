package com.latam.ancoff.utils;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;

import org.apache.http.conn.ssl.TrustStrategy;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class HealthCheckUtil {


    public HealthCheckUtil() {
        super();
    }

    /**
     * Method that check url connection to target url
     * 
     * @param uri
     * @return
     * @throws IOException
     * @throws NoSuchAlgorithmException
     * @throws KeyManagementException
     */
    public boolean checkURL(String uri) throws IOException {
        URL url = new URL(uri);
        if (Constants.PROTOCOL_HTTPS.equals(url.getProtocol())) {
            return validHttpsConnection(url);
        }
        return validHttpConnection(url);
    }

    private static boolean validHttpConnection(URL url) throws IOException {
        URLConnection urlConnection = url.openConnection();
        HttpURLConnection httpURLConnection = (HttpURLConnection) urlConnection;
        return validHttpStatus(httpURLConnection.getResponseCode());
    }

    private static boolean validHttpsConnection(URL url) {
        try {
            TrustStrategy acceptingTrustStrategy = (X509Certificate[] chain, String authType) -> true;
            SSLContext sslContext = org.apache.http.ssl.SSLContexts.custom()
                    .loadTrustMaterial(null, acceptingTrustStrategy).build();
            HttpsURLConnection con = (HttpsURLConnection) url.openConnection();
            con.setSSLSocketFactory(sslContext.getSocketFactory());
            return validHttpStatus(con.getResponseCode());
        } catch (KeyStoreException | NoSuchAlgorithmException | KeyManagementException | IOException e) {
            log.error("Error al conectar", e);
            return false;
        }
    }

    public static boolean validHttpStatus(int codeResult) {
        return (codeResult != Constants.HTTP_STATUS_404 || codeResult != Constants.HTTP_STATUS_UNAVAILABLE);
    }
}
