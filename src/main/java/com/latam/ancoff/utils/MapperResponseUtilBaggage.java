package com.latam.ancoff.utils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.latam.ancoff.entities.AncOfferRQ;
import com.latam.ancoff.entities.AncOfferRS;
import com.latam.ancoff.entities.AncillaryElement;

public final class MapperResponseUtilBaggage {

    private MapperResponseUtilBaggage() {
        throw new IllegalStateException("Utility class");
    }
    
    /**
     * 
     * Method order baggage by priority
     * 
     * @param ancOfferRQ
     * @param ancOfferRS
     * @param property
     */
    public static void isSortAncillaryElement(AncOfferRQ ancOfferRQ, AncOfferRS ancOfferRS, AppProperties property) {
        if( UtilsHeaders.isBaggageAncillary(ancOfferRQ) && !UtilsHeaders.isGroupByAncillary(ancOfferRQ) ) {
            
            List<AncillaryElement> listMainBaggage = ancOfferRS.getGetAncillaryOffersRS().getAncillaryElements()
                    .getAncillaryElement().stream().filter(val -> property.getBaggageOrderList().contains(val.getSubCode()))
                    .sorted(new SortByAncillaryElement(property.getBaggageOrderList())).collect(Collectors.toList());
            List<AncillaryElement> listOtherBaggage = ancOfferRS.getGetAncillaryOffersRS().getAncillaryElements()
                    .getAncillaryElement().stream().filter(val -> !property.getBaggageOrderList().contains(val.getSubCode()))
                    .sorted(new SortOtherBaggage()).collect(Collectors.toList());
            listOtherBaggage.forEach(anc -> anc.setReasonCode(null));
            
            MapperResponseUtilBaggage.setNumberBaggage(ancOfferRQ, listMainBaggage);
            
            ancOfferRS.getGetAncillaryOffersRS().getAncillaryElements().getAncillaryElement().clear();
            ancOfferRS.getGetAncillaryOffersRS().getAncillaryElements().getAncillaryElement().addAll(listMainBaggage);
            ancOfferRS.getGetAncillaryOffersRS().getAncillaryElements().getAncillaryElement().addAll(listOtherBaggage);
            
        }
    }

    public static void setNumberBaggage(AncOfferRQ ancOfferRQ, List<AncillaryElement> listAncillaryElement) {
        if (!UtilsHeaders.isGroupByAncillary(ancOfferRQ) && !listAncillaryElement.isEmpty()) {
            Map<String, Integer> mapNumber = new HashMap<>();
            listAncillaryElement.forEach(ancillaryElementClone -> {
                String key = ancillaryElementClone.getPassenger().getPaxNameNumber()
                        + ancillaryElementClone.getPortions().getPortion().get(0).getDepartureAirport()
                        + ancillaryElementClone.getPortions().getPortion().get(0).getArrivalAirport();
                if (mapNumber.containsKey(key)) {
                    int numberBag = mapNumber.get(key);
                    numberBag++;
                    ancillaryElementClone.setNumber(String.valueOf(numberBag));
                    mapNumber.put(key, numberBag);
                } else {
                    mapNumber.put(key, 1);
                    ancillaryElementClone.setNumber("1");
                }
            });
        }
    }

}
