package com.latam.ancoff.utils;

import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public final class PrintUtil {
    
    
    
    private PrintUtil() {
        super();
    }

    public static String jaxbToString( final Object jaxbObject ){
        
        if (jaxbObject!=null){
            try{
                final JAXBContext context = JAXBContext.newInstance(jaxbObject.getClass());
                final Marshaller marshaller = context.createMarshaller();
                marshaller.setProperty(Marshaller.JAXB_FRAGMENT, true);
                marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
                final StringWriter writer = new StringWriter();
                marshaller.marshal( jaxbObject, writer );
                return writer.toString();
            }catch(final JAXBException e ){
                log.error("Can't marshal an object of type " + jaxbObject.getClass().getName() + " : ", e);
            }
        }
        return "";
    }

}
