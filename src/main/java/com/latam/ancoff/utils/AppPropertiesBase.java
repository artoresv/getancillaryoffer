package com.latam.ancoff.utils;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import lombok.Data;

@Component
@PropertySource("classpath:application.properties")
@Data
public class AppPropertiesBase {
    
    @Value("${ws.error.segments.str}")
    protected String errorSegmentsStr;
    
    @Value("${ws.error.pq.str}")
    protected String errorPqStr;
    
    
    @Value("${baggage.order.list}")
    protected String baggageOrderList;

    
    @Value("${ancillary.request.options.group}")
    protected String ancillaryRequestOptionsGroup;
    
    @Value("${no.ancillary.element}")
    protected String noAncillaryElement;
    
    @Value("${typeCode.info.portion}")
    protected String typeCodeInfoPortion;
    
    @Value("${get.reservation.wsdl}")
    protected String getReservationWSDL;
    
    
    @Value("${get.ancillary.offer.sabre.action}")
    protected String getAncillaryOfferSabreAction;
    
    @Value("${get.ancillary.offer.service.type}")
    protected String getAncillaryOfferSabreType;
    
    @Value("${get.ancillary.offer.service}")
    protected String getAncillaryOfferService;
    
    @Value("${get.ancillary.offer.version}")
    protected String getAncillaryOfferVersion;
    
    @Value("${get.ancillary.offer.wsdl}")
    protected String getAncillaryOfferWSDL;
}
