package com.latam.ancoff.utils;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Component
@PropertySource("classpath:application.properties")
@EqualsAndHashCode(callSuper=false)
@Data
public class AppProperties extends AppPropertiesBase{

    @Value("${session.create.endpoint}")
    private String sessionCreateEndpoint;

    @Value("${session.close.endpoint}")
    public String sessionCloseEndpoint;
    

    @Value("${airline.code}")
    public String airlineCode;
    
    @Value("${reservation.rq.stateful}")
    public String reservationStateful;
    
    @Value("${reservation.rq.default.open.res}")
    public String reservationOpenRes;
    
    @Value("${reservation.rq.stl}")
    public String reservationSTL;

    
    @Value("${ws.successful.str}")
    public String successfulStr;
    
    @Value("${ws.error.securty.token}")
    public String errorSecurityToken;
    
    @Value("${ws.error.not.defined.str}")
    public String errorNotDefinedStr;
    
    @Value("${ws.error.conection.str}")
    public String errorConnectionStr;
    
    @Value("${ws.error.parameters.input}")
    public String errorParametersInput;
    
    @Value("${vcr.display.sabre.action}")
    public String vcrDisplaySabreAction;
    
    @Value("${vcr.display.service.type}")
    public String vcrDisplaySabreType;
    
    @Value("${vcr.display.service}")
    public String vcrDisplayService;
    
    @Value("${vcr.display.version}")
    public String vcrDisplayVersion;
    
    @Value("${vcr.display.wsdl}")
    public String vcrDisplayWSDL;
    
    
    @Value("${get.reservation.sabre.action}")
    public String getReservationSabreAction;
    
    @Value("${get.reservation.service.type}")
    public String getReservationSabreType;
    
    @Value("${get.reservation.service}")
    public String getReservationService;
    
    @Value("${get.reservation.version}")
    public String getReservationVersion;
   

}
