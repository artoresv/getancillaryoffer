package com.latam.ancoff.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.latam.ancoff.entities.AncOfferRQ;
import com.latam.ancoff.entities.AncOfferRS;
import com.latam.ancoff.entities.AncillaryElement;
import com.latam.ancoff.entities.AncillaryElements;
import com.latam.ancoff.entities.GetReservationResponse;
import com.latam.ancoff.entities.Passenger;
import com.latam.ancoff.entities.Portion;
import com.latam.ancoff.entities.Portions;
import com.latam.ancoff.entities.PriceAdditionalBag;
import com.latam.ancoff.entities.ServiceStatus;
import com.latam.ancoff.entities.SummaryBagInformation;
import com.latam.pax.ancillary.offer.Ancillary;
import com.latam.pax.ancillary.offer.EnumServiceTypes;
import com.latam.pax.ancillary.offer.GetAncillaryOffersRQ;
import com.latam.pax.ancillary.offer.GetAncillaryOffersRQ.QueryByItinerary.QueryPassengerItinerary;
import com.latam.pax.ancillary.offer.GetAncillaryOffersRS;
import com.latam.pax.ancillary.offer.Offers;
import com.latam.pax.ancillary.offer.PassengerOffers;
import com.latam.pax.ancillary.offer.ProductDefinition;
import com.latam.pax.getreservation.PassengerPNRB;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public final class MapperResponseUtil {

    private static boolean isPortionInfo;
    private static Ancillary ancillaryAux;

    private MapperResponseUtil() {
    }

    public static void mapperAncOfferRS(AncOfferRQ ancOfferRQ, AncOfferRS ancOfferRS, GetAncillaryOffersRS getAncillaryOffersRS,
            GetAncillaryOffersRQ getAncillaryOffersRQ, GetReservationResponse reservationRS, AppProperties property) {
        log.info("[INICIO] mapperAncOfferRS");
        
        if (null != getAncillaryOffersRS && null != getAncillaryOffersRS.getAncillaryDefinition()
                && null != getAncillaryOffersRS.getOffers()) {
            
            ancOfferRS.setGetAncillaryOffersRS(new com.latam.ancoff.entities.GetAncillaryOffersRS());
            ancOfferRS.getGetAncillaryOffersRS().setAncillaryElements(new AncillaryElements());
            ancOfferRS.getGetAncillaryOffersRS().getAncillaryElements()
                    .setAncillaryElement(new ArrayList<AncillaryElement>());
            
            getAncillaryOffersRS.getAncillaryDefinition().forEach(productDefinition -> {
                AncillaryElement ancillaryElement = new AncillaryElement();
                mapperAncillaryElement(ancillaryElement, productDefinition);

                isPortionInfo = MapperResponseUtilExtended.isPortionInfoSegments(ancillaryElement.getTypeCode(), property);

                List<Ancillary> ancillaryList = ancillaryList(getAncillaryOffersRS, property, productDefinition, ancillaryElement);
                if ( !ancillaryList.isEmpty() ) {
                    List<Offers> offersTempList = new ArrayList<>();
                    isValidNewPortion(ancOfferRQ, ancillaryElement, true);
                    ancillaryList.forEach(ancillary -> {
                      ancillaryElement.setSegmentIndicatorCode(ancillary.getSectorPortionInd());
                      ancillaryAux = ancillary;
                      List<Offers> offersList = getAncillaryOffersRS.getOffers().stream().filter(value -> value.getAncillaryRef().equals(ancillary)).collect(Collectors.toList());
                      offersTempList.addAll(offersList);
                      mapperOffers(ancOfferRQ, ancOfferRS, ancillaryElement, offersList, getAncillaryOffersRQ, getAncillaryOffersRS, reservationRS);
                      
                    });
                    if (UtilsHeaders.isGroupByAncillary(ancOfferRQ)) {
                        mapperAncillaryPaxClone(ancOfferRS, getAncillaryOffersRS, offersTempList, ancillaryElement, reservationRS, getAncillaryOffersRQ);
                    }
                }
            });

            mapper0JO(ancOfferRS, getAncillaryOffersRQ);
            
            MapperResponseUtilBaggage.isSortAncillaryElement(ancOfferRQ, ancOfferRS, property);
            
            ancOfferRS.getGetAncillaryOffersRS().setServiceStatus(new ServiceStatus());
            ancOfferRS.getGetAncillaryOffersRS().getServiceStatus().setCode(Constants.WS_SUCCESSFUL_CODE);
            ancOfferRS.getGetAncillaryOffersRS().getServiceStatus().setMessage(property.getSuccessfulStr());

        } else {
            log.info("Response Ancillary Offers is null");
        }
        log.info("[FIN] mapperAncOfferRS");
    }
    
    private static void isValidNewPortion(AncOfferRQ ancOfferRQ, AncillaryElement ancillaryElement, boolean groupValid) {
        if (UtilsHeaders.isGroupByAncillary(ancOfferRQ) == groupValid) {
            ancillaryElement.setPortions(new Portions());
            ancillaryElement.getPortions().setPortion(new ArrayList<Portion>());
        }
    }

    private static List<Ancillary> ancillaryList(GetAncillaryOffersRS getAncillaryOffersRS, AppProperties property, 
            ProductDefinition productDefinition, AncillaryElement ancillaryElement){
        return getAncillaryOffersRS.getAncillary().stream()
                .filter(value ->  isValidAncillary(value, productDefinition, property) 
                        &&(!Constants.BAGS_SUBCOD_0C3.equalsIgnoreCase(ancillaryElement.getSubCode()) || 
                        (Constants.BAGS_SUBCOD_0C3.equalsIgnoreCase(ancillaryElement.getSubCode())
                                && EnumServiceTypes.C.equals(value.getServiceType().getValue()))))
                .collect(Collectors.toList());
        
    }
    
    private static boolean isValidAncillary(Ancillary value,  ProductDefinition productDefinition, AppProperties property) {
        
        
        return value.getAncillaryDefinitionRef().equals(productDefinition)
                && !property.getNoAncillaryElement().equalsIgnoreCase(value.getAirExtraStatus());
    }
    
    private static void mapperPriceAdditionalBag(PriceAdditionalBag priceAdditionalBag,
            AncillaryElement ancillaryElement) {
        priceAdditionalBag.setAncillaryGroupCode(ancillaryElement.getGroupCode());
        priceAdditionalBag.setAncillaryCode(ancillaryElement.getCode());
        priceAdditionalBag.setAncillarySubCode(ancillaryElement.getSubCode());
        priceAdditionalBag.setAncillaryDescription(ancillaryElement.getDescription());
    }

    /**
     * 
     * Method mapping Ancillary element
     * 
     * @param ancillaryElement
     * @param productDefinition
     */
    private static void mapperAncillaryElement(AncillaryElement ancillaryElement, ProductDefinition productDefinition) {
        ancillaryElement.setTypeCode(productDefinition.getElectronicMiscDocType().getCode());
        ancillaryElement.setGroupCode(productDefinition.getGroup());
        ancillaryElement.setCode(productDefinition.getReasonForIssuance().getCode());
        ancillaryElement.setSubCode(productDefinition.getSubCode());
        ancillaryElement.setDescription(productDefinition.getCommercialName());
        ancillaryElement.setReferenceNumber("0");
        ancillaryElement.setQuantity(0);
        ancillaryElement.setStatusCode(null);
    }

    /**
     * 
     * Method mapping Ancillary element and Pax Info
     * 
     * @param ancOfferRS
     * @param getAncillaryOffersRS
     * @param offers
     * @param ancillaryElement
     * @param reservationRS
     */
    private static void mapperAncillaryPaxClone(AncOfferRS ancOfferRS, GetAncillaryOffersRS getAncillaryOffersRS,
            List<Offers> offersList, AncillaryElement ancillaryElement, GetReservationResponse reservationRS,
            GetAncillaryOffersRQ getAncillaryOffersRQ) {

        List<PassengerOffers> passengerOffersList = getAncillaryOffersRS.getPassengerOffers().stream()
                .filter(value -> value.getOfferRefs().stream().anyMatch(offersList::contains)).collect(Collectors.toList());
        if (!passengerOffersList.isEmpty()) {

            for (PassengerOffers passengerOffers : passengerOffersList) {

                    AncillaryElement ancillaryElementClone = new AncillaryElement(ancillaryElement);

                    Passenger passenger = new Passenger();
                    passenger.setPaxNameNumber(passengerOffers.getPassengerReference().getNameReferenceNumber());
                    PassengerPNRB passengerPNRB = reservationRS.getPassengers().stream()
                            .filter(value -> value.getNameId()
                                    .equalsIgnoreCase(passengerOffers.getPassengerReference().getNameReferenceNumber()))
                            .findAny().orElse(new PassengerPNRB());
                    passenger.setFirstName(passengerPNRB.getFirstName());
                    passenger.setLastName(passengerPNRB.getLastName());

                    ancillaryElementClone.setPassenger(passenger);
                    ancillaryElementClone.setReasonCode("0");

                    mapperBags(ancOfferRS, ancillaryElementClone, getAncillaryOffersRQ, ancillaryAux);

            }
        }
    }
    

    /**
     * 
     * Method mapping offers
     * 
     * @param ancillaryElement
     * @param offersList
     * @param priceAdditionalBag
     * @param getAncillaryOffersRQ
     * @param getAncillaryOffersRS
     * @param reservationRS
     * @param ancOfferRS
     */
    private static void mapperOffers(AncOfferRQ ancOfferRQ, AncOfferRS ancOfferRS, AncillaryElement ancillaryElement, List<Offers> offersList,
            GetAncillaryOffersRQ getAncillaryOffersRQ, GetAncillaryOffersRS getAncillaryOffersRS, GetReservationResponse reservationRS) {

        if (!offersList.isEmpty()) {

            if (!isPortionInfo) {
                // Type code other than 1, the first item in the list is left
                Offers offer = offersList.get(0);
                offersList.clear();
                offersList.add(offer);
            }

            offersList.forEach(offers -> {
                isValidNewPortion(ancOfferRQ, ancillaryElement, false);
                Portion portion = new Portion();
                List<Offers> offersTempList = new ArrayList<>();
                if (isPortionInfo) {
                    portion.setDepartureAirport(offers.getOrigin());
                    portion.setArrivalAirport(offers.getDestination());
                }

                PriceAdditionalBag priceAdditionalBag = new PriceAdditionalBag();
                    
                MapperResponseUtilExtended.mapperPortion(offers, portion, priceAdditionalBag, getAncillaryOffersRQ, isPortionInfo);

                if (Constants.BAGS_SUBCOD_0JO.equalsIgnoreCase(ancillaryElement.getSubCode())) {
                    ancillaryElement.setPriceAdditionalBag(priceAdditionalBag);
                    mapperPriceAdditionalBag(priceAdditionalBag, ancillaryElement);
                    
                }
                
                ancillaryElement.getPortions().getPortion().add(portion);
                if (!UtilsHeaders.isGroupByAncillary(ancOfferRQ)) {
                    offersTempList.add(offers);
                    mapperAncillaryPaxClone(ancOfferRS, getAncillaryOffersRS, offersTempList, ancillaryElement, reservationRS, getAncillaryOffersRQ);
                }

            });
            
        }
    }

    private static void mapperBags(AncOfferRS ancOfferRS, AncillaryElement ancillaryElementClone,
            GetAncillaryOffersRQ getAncillaryOffersRQ, Ancillary ancillary) {

        switch (ancillaryElementClone.getSubCode()) {
        case "0C3":

            mapper0C3(getAncillaryOffersRQ, ancOfferRS, ancillaryElementClone);
            break;

        case "0DF":
            mapper0Df(ancOfferRS, ancillaryElementClone, ancillary);
            break;
        default:
            ancOfferRS.getGetAncillaryOffersRS().getAncillaryElements().getAncillaryElement()
                    .add(ancillaryElementClone);
            break;

        }

    }

    private static void mapper0Df(AncOfferRS ancOfferRS, AncillaryElement ancillaryElementClone,
           Ancillary ancillary) {
        
        if(null != ancillary.getBaggageAllowance() && null != ancillary.getBaggageAllowance().getMaxPieces()) {
                ancillaryElementClone.setReasonCode("1");
                for (int x = 0; x < ancillary.getBaggageAllowance().getMaxPieces().getValue(); x++) {
                AncillaryElement ancillaryElementCloneAux = new AncillaryElement(ancillaryElementClone);
                
                    ancOfferRS.getGetAncillaryOffersRS().getAncillaryElements().getAncillaryElement()
                        .add(ancillaryElementCloneAux);
                }
            }
        
    }

    private static void mapper0C3(GetAncillaryOffersRQ getAncillaryOffersRQ, AncOfferRS ancOfferRS,
            AncillaryElement ancillaryElementClone) {
        getAncillaryOffersRQ.getQueryByItinerary().getQueryPassengerItinerary().forEach(queryPassengerItinerary -> {
            
                    if (null != queryPassengerItinerary.getPassenger().getLoyaltyAccounts()) {
                        ancillaryElementClone.getPortions().getPortion().forEach(portion -> {
                            if (ValidationUtils.validateAmountZero(portion.getPrice().getBaseAmount().getText())) {
                                ancillaryElementClone.setReasonCode("2");
                            }
                        });
                    }
                });

        ancOfferRS.getGetAncillaryOffersRS().getAncillaryElements().getAncillaryElement()
        .add(ancillaryElementClone);

    }

    private static void mapper0JO(AncOfferRS ancOfferRS, GetAncillaryOffersRQ getAncillaryOffersRQ) {

        List<AncillaryElement> ancillaryElement0JO = ancOfferRS.getGetAncillaryOffersRS().getAncillaryElements().getAncillaryElement().stream()
        .filter(ancillaryElement -> ancillaryElement.getSubCode().equals(Constants.BAGS_SUBCOD_0JO))
        .collect(Collectors.toList());
        
        
        ancillaryElement0JO.forEach(ancillaryOJO -> {

            List<AncillaryElement> ancillaryElement0DF = ancOfferRS.getGetAncillaryOffersRS().getAncillaryElements().getAncillaryElement().stream()
                        .filter(ancillary -> isPortion0DF(ancillaryOJO, ancillary, Constants.BAGS_SUBCOD_0DF)) 
                    .collect(Collectors.toList());
            
            List<AncillaryElement> ancillaryElement0C3 = ancOfferRS.getGetAncillaryOffersRS().getAncillaryElements().getAncillaryElement().stream()
                        .filter(ancillary -> isPortion0C3(ancillaryOJO, ancillary, getAncillaryOffersRQ, Constants.BAGS_SUBCOD_0C3))
                    .collect(Collectors.toList());
            
                ancillaryOJO.setSummaryBagInformation(summaryInformationBag(String.valueOf(ancillaryElement0C3.size()), String.valueOf(ancillaryElement0DF.size())));
        });
            
        isValidSummaryBagInformation0DF(ancillaryElement0JO, ancOfferRS);
            
    }
    
    private static Boolean isPortion0DF(AncillaryElement ancillary0JO, AncillaryElement ancillaryElement, String subCode) {
        
        Boolean isValid = Boolean.FALSE;
        
            for(Portion portion0JO : ancillary0JO.getPortions().getPortion()){

                for(Portion portion : ancillaryElement.getPortions().getPortion()){
                    if(null != portion0JO && null != portion.getArrivalAirport()) {
                        isValid = ancillaryElement.getSubCode().equals(subCode)
                                && ancillaryElement.getPassenger().getPaxNameNumber().equals(ancillary0JO.getPassenger().getPaxNameNumber()) 
                                && portion.getArrivalAirport().equalsIgnoreCase(portion0JO.getArrivalAirport())
                                && portion.getDepartureAirport().equalsIgnoreCase(portion0JO.getDepartureAirport());
                    }
                                
                }
            }
         
        return isValid;
    }
    
    private static Boolean isPortion0C3(AncillaryElement ancillary0JO, AncillaryElement ancillaryElement, 
            GetAncillaryOffersRQ getAncillaryOffersRQ, String subCode) {
        
        Boolean isValid = Boolean.FALSE;
        
            for(Portion portion0JO : ancillary0JO.getPortions().getPortion()){
                
                for(Portion portion : ancillaryElement.getPortions().getPortion()){
                    if(null != portion0JO && null != portion.getArrivalAirport()) {
                        isValid = ancillaryElement.getPassenger().getPaxNameNumber().equals(ancillary0JO.getPassenger().getPaxNameNumber()) 
                                && portion.getArrivalAirport().equalsIgnoreCase(portion0JO.getArrivalAirport())
                                && portion.getDepartureAirport().equalsIgnoreCase(portion0JO.getDepartureAirport())
                                && isValidPassengerFQTV(getAncillaryOffersRQ, ancillaryElement, portion, subCode) ;
                                
                }
            }
        }

        return isValid;
    }
 
    private static boolean isValidPassengerFQTV(GetAncillaryOffersRQ getAncillaryOffersRQ, 
            AncillaryElement ancillaryElement, Portion portion, String subCode) {
        boolean isValidFQTV = Boolean.FALSE;
        
        for(QueryPassengerItinerary queryPassengerItinerary : getAncillaryOffersRQ.getQueryByItinerary().getQueryPassengerItinerary()){
                boolean isValid = null != portion.getPrice() && null != portion.getPrice().getTotalAmount()
                    && Double.parseDouble(portion.getPrice().getTotalAmount().getText()) == 0
                    && ancillaryElement.getSubCode().equals(subCode);
            if(isValid 
               && null != queryPassengerItinerary.getPassenger().getLoyaltyAccounts()
               && queryPassengerItinerary.getPassenger().getNameReferenceNumber()
                .equalsIgnoreCase(ancillaryElement.getPassenger().getPaxNameNumber())) {
                isValidFQTV = Boolean.TRUE;
            }
}
        
        return isValidFQTV;
    }

    private static void isValidSummaryBagInformation0DF(List<AncillaryElement> ancillaryElement0JO, AncOfferRS ancOfferRS) {

        
        ancillaryElement0JO.forEach(ancillaryOJO -> {  
            
            List<AncillaryElement> ancillaryElement0DF = ancOfferRS.getGetAncillaryOffersRS().getAncillaryElements()
                    .getAncillaryElement().stream().filter(ancillary -> isPortion0DFSummaryBagInformation(ancillaryOJO,
                            ancillary, Constants.BAGS_SUBCOD_0DF))
                    .collect(Collectors.toList()); 

            if(null != ancillaryElement0DF && !ancillaryElement0DF.isEmpty()) {
             //obtenemos el ultimo objeto de la lista 0DF
            ancillaryElement0DF.get((ancillaryElement0DF.size() -1))
            .setSummaryBagInformation(summaryInformationBag("0", String.valueOf(ancillaryElement0DF.size())));
            }
            
            
        });
        
    }
    
    private static Boolean isPortion0DFSummaryBagInformation(AncillaryElement ancillary0JO, AncillaryElement ancillaryElement, String subCode) {
        
        Boolean isValid = Boolean.FALSE;
        
            for(Portion portion0JO : ancillary0JO.getPortions().getPortion()){
                
                for(Portion portion : ancillaryElement.getPortions().getPortion()){
                    if(null != portion0JO && null != portion.getArrivalAirport()) {
                        isValid = ancillaryElement.getSubCode().equals(subCode)
                                && ancillaryElement.getPassenger().getPaxNameNumber().equals(ancillary0JO.getPassenger().getPaxNameNumber()) 
                                && !portion.getArrivalAirport().equalsIgnoreCase(portion0JO.getArrivalAirport())
                                && !portion.getDepartureAirport().equalsIgnoreCase(portion0JO.getDepartureAirport());
}

                }
            }
         
        return isValid;
    }

    private static SummaryBagInformation summaryInformationBag(String count0C3, String count0DF) {
    
        SummaryBagInformation summaryBagInformation = new SummaryBagInformation();
        
        summaryBagInformation.setPaidBags("0");
        
        // cantidad 0C3 con precio 0 y pax FQTV
        summaryBagInformation.setBagsByffpAllowance(count0C3);

        // cantidad maletas 0DF
        summaryBagInformation.setBagsByAllowance(count0DF);
        
        // suma de bagsByAllowance + paidbags (0) + bagsByffpAllowance
        int totalBags = (Integer.parseInt(summaryBagInformation.getBagsByffpAllowance())
                + Integer.parseInt(summaryBagInformation.getBagsByAllowance()));
        
        summaryBagInformation.setTotalBags(String.valueOf(totalBags));
        
        return summaryBagInformation;
    }
}




