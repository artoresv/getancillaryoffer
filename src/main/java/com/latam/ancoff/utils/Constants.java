package com.latam.ancoff.utils;

import java.util.Locale;

public final class Constants {

    public static final int LENGTH_PNR = 6;
    public static final int LENGTH_FAREBASIS = 8;
    public static final int WS_SUCCESSFUL_CODE = 0;
    public static final int WS_ERROR_NOT_DEFINED_CODE = -1;
    public static final int WS_ERROR_OPEN_SESSION_CODE = -2;
    public static final int WS_ERROR_CLOSE_SESSION_CODE = -3;
    public static final int WS_ERROR_HEADER_REQUEST_CODE = -4;
    public static final int WS_ERROR_INPUT_PARAMETERS_CODE = -5;
    public static final int WS_ERROR_WEBSERVICE_CODE = -6;
    public static final int WS_ERROR_CONNECTION_CODE = -23;
    public static final int WS_ERROR_SEGMENTS_CODE = -88;
    public static final int WS_ERROR_PQ_CODE = -99;

    public static final String WS_SUCCESSFUL_STR = "ws.successful.str";
    public static final String WS_ERROR_NOT_DEFINED_STR = "ws.error.not.defined.str";
    public static final String WS_ERROR_SECURITY_TOKEN = "ws.error.securty.token";
    public static final String WS_ERROR_CONNECTION_STR = "ws.error.conection.str";

    public static final String MESSAGE_HEADER_VERSION = "1.0";

    public static final String DATE_FULL_FORMAT = "yyyy-MM-dd'T'HH:mm:ss";
    public static final String DATE_MID_FORMAT = "'mid:'yyyyMMdd'-'HHmmss'@lan.com'";
    public static final Locale DEFAULT_LOCALE = new Locale("en", "US");
    public static final Locale LOCALE = new Locale(Constants.LOCALE_MIN, Constants.LOCALE_US_MAY);

    public static final String AIRLINE_CODE = "AirlineCode";
    public static final String PASSENGER = "P";
    public static final String FARE_ID = "FareID";
    public static final String FLIGHT = "flight_";
    public static final String SEGMENT = "SEG";
    public static final String STRING_EMPTY = "";
    public static final String PASSENGER_CLASS_STANDARD = "S";
    public static final String PASSENGER_CLASS_INFANT = "I";
    public static final String PASSENGER_CLASS_CHILD = "CNN";
    public static final String PASSENGER_ADULT = "ADT";
    public static final String PASSENGER_INFANT = "INF";
    public static final String PASSENGER_CHILD = "CHD";
    

    public static final String COMMA = ",";

    public static final String BAGS_SUBCOD_0JO = "0JO";
    public static final String BAGS_SUBCOD_0DF = "0DF";
    public static final String BAGS_SUBCOD_0C3 = "0C3";
    public static final String ANCILLARY_TYPECODE_C = "C";
    
    public static final String STATEFUL = "reservation.rq.stateful";
    public static final String DEFAULT_OPEN_RES = "reservation.rq.default.open.res";
    public static final String STL = "reservation.rq.stl";
    public static final String MSG_URI = "URI";
    public static final String MSG_SERVICE = "Service=";
    public static final String LOCALE_MIN = "en";
    public static final String LOCALE_US_MAY = "US";
    public static final String CLIENT_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss";
    public static final String CLIENT_DATE_FORMAT_MID = "'mid:'yyyyMMdd'-'HHmmss'@lan.com'";

    public static final String ERROR_MESSAGE_TITLE = "ErrorMessage=";
    public static final String NATIVE_MESSAGE_TITLE = "NativeMessage=";
    
    /*
     * Health Check constants
     */

    public static final String HEALTH_SESSION_CREATE= "SESSION_CREATE";
    public static final String HEALTH_SESSION_CLOSE= "SESSION_CLOSE";
    public static final String HEALTH_SERVICES_OK_MESSAGE = "UP";
    public static final String HEALTH_HTTP_ERROR_CODE = "DOWN";
    public static final int HTTP_STATUS_UNAVAILABLE = -1;
    public static final int HTTP_STATUS_404 = 404;
    public static final String PROTOCOL_HTTPS = "https";

    /**
     * Instantiates a new constants.
     */
    private Constants() {
    }

}
