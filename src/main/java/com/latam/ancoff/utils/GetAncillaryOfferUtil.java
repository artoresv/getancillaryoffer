package com.latam.ancoff.utils;

import java.util.List;
import java.util.Map;

import com.latam.ancoff.entities.AncOfferRQ;
import com.latam.ancoff.entities.GetReservationResponse;
import com.latam.host.sabre.ws.WSSabreSessionManager;
import com.latam.pax.ancillary.offer.FareInfo;
import com.latam.pax.ancillary.offer.GetAncillaryOffersRQ.QueryByItinerary;
import com.latam.pax.ancillary.offer.GetAncillaryOffersRQ.QueryByItinerary.QueryPassengerItinerary;
import com.latam.pax.ancillary.offer.GetAncillaryOffersRQ.QueryByItinerary.QueryPassengerItinerary.PassengerItinerary;
import com.latam.pax.ancillary.offer.LoyaltyAccounts;
import com.latam.pax.ancillary.offer.LoyaltyAccounts.LoyaltyAccountWithId;
import com.latam.pax.ancillary.offer.PartyId;
import com.latam.pax.ancillary.offer.Passenger;
import com.latam.pax.ancillary.offer.Security2;
import com.latam.pax.ancillary.offer.Segment;
import com.latam.pax.getreservation.PassengerPNRB;

public final class GetAncillaryOfferUtil {
    
    private GetAncillaryOfferUtil() {

    }

    public static PartyId ancillaryOfferGetParty(WSSabreSessionManager wsSession) {
        PartyId toPartyId = new PartyId();
        toPartyId.setType(Constants.MSG_URI);
        toPartyId.setValue(wsSession.getTo());
        return toPartyId;
    }

    public static PartyId ancillaryOfferGetPartyId(WSSabreSessionManager wsSession) {
        PartyId fromPartyId = new PartyId();
        fromPartyId.setType(Constants.MSG_URI);
        fromPartyId.setValue(wsSession.getFrom());
        return fromPartyId;
    }

    public static Security2 ancillaryOfferGetSecurity(WSSabreSessionManager wsSession) {
        Security2 security = new Security2();
        security.setBinarySecurityToken(wsSession.getBinarySecurityToken());
        return security;
    }

    public static void mapperPassenger(AncOfferRQ ancOfferRQ, GetReservationResponse reservationRS, Map<String, Segment> mapSegment,
            Map<String, List<FareInfo>> mapFareInfo, QueryByItinerary queryByItinerary) {

        if (UtilsHeaders.isGroupByAncillary(ancOfferRQ)) {
            // Type code other than 1, the first item in the list is left
            PassengerPNRB passengerAux = reservationRS.getPassengers().get(0);
            reservationRS.getPassengers().clear();
            reservationRS.getPassengers().add(passengerAux);
        }
        
        for (PassengerPNRB passengerAux : reservationRS.getPassengers()) {
            QueryPassengerItinerary queryPassengerItinerary = new QueryPassengerItinerary();
            PassengerItinerary passengerItinerary = new PassengerItinerary();
            Passenger passenger = new Passenger();
            
            passenger.setId(Constants.PASSENGER + passengerAux.getNameAssocId());
            passenger.setNameReferenceNumber(passengerAux.getNameId());
            
            
            GetAncillaryOfferUtil.setPassengerType(passenger, passengerAux);

            for (Map.Entry<String, Segment> entry : mapSegment.entrySet()) {
                if (mapFareInfo.containsKey(entry.getKey())) {
                    List<FareInfo> listFareInfo = mapFareInfo.get(entry.getKey());

                    MapperGetAncillaryOffer.mapperFareInfo(listFareInfo, entry, passengerItinerary);

                }

            }
            boolean isFrequentFlyer = false;
            if( !passengerAux.getFrequentFlyer().isEmpty() ) {
                //Frequent flyer
                isFrequentFlyer = true;
            }
            
            queryPassengerItinerary.setPassenger(passenger);
            
                
            GetAncillaryOfferUtil.setLoyaltyAccountWithId(reservationRS, passenger.getId(),  queryPassengerItinerary, isFrequentFlyer);

           
            queryPassengerItinerary.getPassengerItinerary().add(passengerItinerary);

            queryByItinerary.getQueryPassengerItinerary().add(queryPassengerItinerary);
            
        }
        
    }

    public static Passenger setPassengerType(Passenger passenger, PassengerPNRB passengerAux) {
        if (Constants.PASSENGER_CLASS_STANDARD.equals(passengerAux.getNameType().toString())
                && (Constants.STRING_EMPTY.equals(passengerAux.getPassengerType())
                        || null == passengerAux.getPassengerType())) {
            passenger.setType(Constants.PASSENGER_ADULT);
        } else if (Constants.PASSENGER_CLASS_STANDARD.equals(passengerAux.getNameType().toString())
                && passengerAux.getPassengerType().equalsIgnoreCase(Constants.PASSENGER_CLASS_CHILD)) {
            passenger.setType(Constants.PASSENGER_CHILD);
        } else if (Constants.PASSENGER_CLASS_INFANT.equals(passengerAux.getNameType().toString())) {
            passenger.setType(Constants.PASSENGER_INFANT);
        }

        return passenger;
    }
    
    public static void setLoyaltyAccountWithId(GetReservationResponse reservationRS, String passagerId, QueryPassengerItinerary queryPassengerItinerary, boolean isFrequentFlyer) {
        
        if (isFrequentFlyer == Boolean.TRUE ) {
            reservationRS.getOpenReservationElementTypes().forEach(openReservationElement -> {
                if(null != openReservationElement.getLoyalty() ) {
                    
                    LoyaltyAccountWithId loyaltyAccount  = new LoyaltyAccountWithId();
                    queryPassengerItinerary.getPassenger().setLoyaltyAccounts(new LoyaltyAccounts());
                    
                    loyaltyAccount.setMemberAirline(openReservationElement.getLoyalty().getFrequentFlyer().getReceivingCarrierCode());
                    loyaltyAccount.setMemberId(openReservationElement.getLoyalty().getFrequentFlyer().getFrequentFlyerNumber());
                    loyaltyAccount.setEliteStatus(openReservationElement.getLoyalty().getFrequentFlyer().getBanner());
                    loyaltyAccount.setTierTag(openReservationElement.getLoyalty().getFrequentFlyer().getTag());
                    loyaltyAccount.setTierNumber(openReservationElement.getLoyalty().getFrequentFlyer().getVitType().toString());
                    loyaltyAccount.setId(passagerId+"_ff");
            
                    queryPassengerItinerary.getPassenger().getLoyaltyAccounts().getLoyaltyAccountWithId().add(loyaltyAccount);
                }
            });
        }
    }
    
    
}
