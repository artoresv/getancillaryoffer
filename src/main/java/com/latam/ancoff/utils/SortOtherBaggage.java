package com.latam.ancoff.utils;

import java.io.Serializable;
import java.util.Comparator;

import com.latam.ancoff.entities.AncillaryElement;

public class SortOtherBaggage implements Serializable, Comparator<AncillaryElement>{

    private static final long serialVersionUID = -2518607271334562489L;

    public SortOtherBaggage() {
     // Do nothing because constructor is empty
    }

    @Override
    public int compare(AncillaryElement o1, AncillaryElement o2) {
        if (o1.getPassenger().getPaxNameNumber().compareTo(o2.getPassenger().getPaxNameNumber()) == 0) {
            boolean isNotNullSegmentNumber = null != o1.getPortions().getPortion().get(0).getSegmentNumber()
                    && null != o2.getPortions().getPortion().get(0).getSegmentNumber();
            if (isNotNullSegmentNumber) {
                return validateSegmentAndSubCode(o1, o2);
            } else {
                return 1;
            }
        } else {
            return validatePaxNameNumber(o1, o2);
        }
    }
    
    /**
     * 
     * Validate Segment and SubCode BG
     * 
     * @param o1
     * @param o2
     * @return
     */
    private int validateSegmentAndSubCode(AncillaryElement o1, AncillaryElement o2) {
        if (o1.getPortions().getPortion().get(0).getSegmentNumber()
                .compareTo(o2.getPortions().getPortion().get(0).getSegmentNumber()) == 0) {
            return o1.getSubCode().compareTo(o2.getSubCode());
        } else {
            return validateSegmentNumber(o1, o2);
        }
    }
    
    
    /**
     * 
     * Validate PaxNameNumber
     * 
     * @param o1
     * @param o2
     * @return
     */
    private int validatePaxNameNumber(AncillaryElement o1, AncillaryElement o2) {
        return o1.getPassenger().getPaxNameNumber().compareTo(o2.getPassenger().getPaxNameNumber());
    }
    
    /**
     * 
     * Validate Segment Number
     * 
     * @param o1
     * @param o2
     * @return
     */
    private int validateSegmentNumber(AncillaryElement o1, AncillaryElement o2) {
        return o1.getPortions().getPortion().get(0).getSegmentNumber()
                    .compareTo(o2.getPortions().getPortion().get(0).getSegmentNumber());
    }

}
