package com.latam.ancoff.utils;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import com.latam.ancoff.entities.AncOfferRQ;
import com.latam.pax.ancillary.offer.AncillaryRequestOptions;
import com.latam.pax.ancillary.offer.FareBreakAssociation;
import com.latam.pax.ancillary.offer.FareInfo;
import com.latam.pax.ancillary.offer.GetAncillaryOffersRQ;
import com.latam.pax.ancillary.offer.PassengerSegment;
import com.latam.pax.ancillary.offer.Segment;
import com.latam.pax.ancillary.offer.GetAncillaryOffersRQ.QueryByItinerary.QueryPassengerItinerary.PassengerItinerary;

public class MapperGetAncillaryOfferBase {

    protected  MapperGetAncillaryOfferBase() {
        
    }
    
    protected static void mapperFareInfo(List<FareInfo> listFareInfo, Map.Entry<String, Segment> entry,
            PassengerItinerary passengerItinerary) {

        PassengerSegment passengerSegment = new PassengerSegment();
        passengerSegment.setSegmentRef(entry.getValue());
        FareBreakAssociation fareBreakAssociation = new FareBreakAssociation();
        fareBreakAssociation.setFareInfoRef(listFareInfo.get(0));
        passengerSegment.getFareBreakAssociation().add(fareBreakAssociation);
        passengerItinerary.getPassengerSegment().add(passengerSegment);

    }
    
    /**
     * 
     * Mapping Ancillary Request Options
     * 
     * @param request
     * @param property
     */
    protected static void mapperAnillaryOptions(GetAncillaryOffersRQ request, AncOfferRQ ancOfferRQ,
            AppProperties property) {

        if (null != ancOfferRQ.getGetAncillaryOffersRQ() 
                && null != ancOfferRQ.getGetAncillaryOffersRQ().getFilter()
                && null != ancOfferRQ.getGetAncillaryOffersRQ().getFilter().getAncillaryElements() && !ancOfferRQ
                        .getGetAncillaryOffersRQ().getFilter().getAncillaryElements().getAncillaryElement().isEmpty()) {
            ancOfferRQ.getGetAncillaryOffersRQ().getFilter().getAncillaryElements().getAncillaryElement()
                    .forEach(ancillaryElement -> {
                        AncillaryRequestOptions ancillaryRequestOptions = new AncillaryRequestOptions();
                        ancillaryRequestOptions.setGroup(ancillaryElement.getGroupCode());
                        request.getAncillaryRequestOptions().add(ancillaryRequestOptions);
                    });
        } else {

            if (null != property.getAncillaryRequestOptionsGroup()
                    && !"".equalsIgnoreCase(property.getAncillaryRequestOptionsGroup())) {
                Arrays.stream(property.getAncillaryRequestOptionsGroup().split(Constants.COMMA)).forEach(group -> {
                    AncillaryRequestOptions ancillaryRequestOptions = new AncillaryRequestOptions();
                    ancillaryRequestOptions.setGroup(group);
                    request.getAncillaryRequestOptions().add(ancillaryRequestOptions);
                });

            }

        }
    }
    
    
}
