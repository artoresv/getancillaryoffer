package com.latam.ancoff.utils;

import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import org.ebxml.namespaces.messageheader.PartyId;
import org.xmlsoap.schemas.ws._2002._12.secext.Security;

import com.latam.ancoff.entities.AncOfferRQ;
import com.latam.host.sabre.ws.WSSabreSessionManager;
import https.webservices_sabre_com.websvc.VCRDisplayService;

public final class UtilsHeaders {

    private static VCRDisplayService service;

    private UtilsHeaders() {

    }

    public static PartyId getParty(WSSabreSessionManager wsSession) {
        PartyId toPartyId = new PartyId();
        toPartyId.setType(Constants.MSG_URI);
        toPartyId.setValue(wsSession.getTo());
        return toPartyId;
    }

    public static PartyId getPartyId(WSSabreSessionManager wsSession) {
        PartyId fromPartyId = new PartyId();
        fromPartyId.setType(Constants.MSG_URI);
        fromPartyId.setValue(wsSession.getFrom());
        return fromPartyId;
    }

    public static Security getSecurity(WSSabreSessionManager wsSession) {
        Security security = new Security();
        security.setBinarySecurityToken(wsSession.getBinarySecurityToken());
        return security;
    }

    /**
     * Gets the service.
     *
     * @param wsdl the wsdl
     * @return the service
     * @throws MalformedURLException     the malformed URL exception
     * @throws AppConfigException        the app config exception
     * @throws DisplayRewardPayException
     */
    public static VCRDisplayService getInstanceService(URL url) {
        synchronized (VCRDisplayService.class) {
            if (null == service) {
                if (null != url) {
                    service = new VCRDisplayService(url,
                            new QName("https://webservices.sabre.com/websvc", "VCR_DisplayService"));
                } else {
                    service = new VCRDisplayService();
                }
            }
        }
        return service;
    }
    
    public static boolean isGroupByAncillary(AncOfferRQ ancOfferRQ) {
        return null != ancOfferRQ.getGetAncillaryOffersRQ() 
                && null != ancOfferRQ.getGetAncillaryOffersRQ().getGroupByAncillary()
                && "true".equalsIgnoreCase(ancOfferRQ.getGetAncillaryOffersRQ().getGroupByAncillary());
    }
    
    public static boolean isBaggageAncillary(AncOfferRQ ancOfferRQ) {
        boolean isFilterBaggage = null != ancOfferRQ.getGetAncillaryOffersRQ() && null != ancOfferRQ.getGetAncillaryOffersRQ().getFilter()
                && null != ancOfferRQ.getGetAncillaryOffersRQ().getFilter().getAncillaryElements();
        if( isFilterBaggage ) {
            return !ancOfferRQ.getGetAncillaryOffersRQ().getFilter().getAncillaryElements().getAncillaryElement()
                    .isEmpty()
            && ancOfferRQ.getGetAncillaryOffersRQ().getFilter().getAncillaryElements().getAncillaryElement()
                    .stream().filter(val -> "BG".equalsIgnoreCase(val.getGroupCode())).count() > 0;
        } else {
            return false;
        }

    }
}
