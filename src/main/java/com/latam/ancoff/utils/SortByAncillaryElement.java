package com.latam.ancoff.utils;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

import com.latam.ancoff.entities.AncillaryElement;

public class SortByAncillaryElement implements Serializable, Comparator<AncillaryElement>{

    private static final long serialVersionUID = 8675173703990875857L;
    
    private Map<String, Integer> subCodePriority = new HashMap<>();
    private int priority;
    
    public SortByAncillaryElement(String baggageOrder) {
        priority = 0;
        Arrays.stream(baggageOrder.split(Constants.COMMA)).forEach(baggage -> {
            priority++;
            subCodePriority.put(baggage, priority);
        });
    }

    @Override
    public int compare(AncillaryElement o1, AncillaryElement o2) {
        boolean isNotNullSegmentNumber = null != o1.getPortions().getPortion().get(0).getSegmentNumber()
                && null != o2.getPortions().getPortion().get(0).getSegmentNumber();
        if (isNotNullSegmentNumber) {
            if (o1.getPortions().getPortion().get(0).getSegmentNumber()
                    .compareTo(o2.getPortions().getPortion().get(0).getSegmentNumber()) == 0) {
                return validatePaxNumberAndSubCode(o1, o2);
            } else {
                return validateSegmentNumber(o1, o2);
            }
        } else {
            return 1;
        }
    }
    
    /**
     * 
     * Validate PaxNumber and SubCode BG
     * 
     * @param o1
     * @param o2
     * @return
     */
    private int validatePaxNumberAndSubCode(AncillaryElement o1, AncillaryElement o2) {
        if (o1.getPassenger().getPaxNameNumber().compareTo(o2.getPassenger().getPaxNameNumber()) == 0) {
            return subCodePriority(o1, o2);
        } else {
            return validatePaxNameNumber(o1, o2);
        }
    }
    
    /**
     * 
     * Validate SubCode by priority
     * 
     * @param o1
     * @param o2
     * @return
     */
    private int subCodePriority(AncillaryElement o1, AncillaryElement o2) {
        if (null != subCodePriority.get(o1.getSubCode()) && null != subCodePriority.get(o2.getSubCode())) {
            return subCodePriority.get(o1.getSubCode()).compareTo(subCodePriority.get(o2.getSubCode()));
        } else {
            return 1;
        }
    }
    
    /**
     * 
     * Validate PaxNameNumber
     * 
     * @param o1
     * @param o2
     * @return
     */
    private int validatePaxNameNumber(AncillaryElement o1, AncillaryElement o2) {
        return o1.getPassenger().getPaxNameNumber().compareTo(o2.getPassenger().getPaxNameNumber());
    }
    
    /**
     * 
     * Validate Segment Number
     * 
     * @param o1
     * @param o2
     * @return
     */
    private int validateSegmentNumber(AncillaryElement o1, AncillaryElement o2) {
        if (!subCodePriority.containsKey(o2.getSubCode())) {
            return -1;
        } else {
            return o1.getPortions().getPortion().get(0).getSegmentNumber()
                    .compareTo(o2.getPortions().getPortion().get(0).getSegmentNumber());
        }
    }

}
