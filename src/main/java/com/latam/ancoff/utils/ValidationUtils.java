package com.latam.ancoff.utils;

import com.latam.ancoff.entities.AncOfferRS;
import com.latam.ancoff.entities.GetAncillaryOffersRS;
import com.latam.ancoff.entities.ServiceStatus;

public final class ValidationUtils {
    
    private static final double THRESHOLD = .0001;
    private static final double ZERO = 0.0;

    /**
     * Instantiates a new constants.
     */
    private ValidationUtils() {
    }

    /**
     * everis-R. Yanez 18/02/2019 Valida si el objeto se encuentra vacio, null, o es
     * "0" dependiento del tipo de dato
     * 
     * @param dato
     * @return Boolean
     */
    public static Boolean validateStringNotNull(Object obj) {

        Boolean isDouble = obj instanceof Double && ((Double) obj).equals((Double.valueOf(0)));
        Boolean isInteger = obj instanceof Integer && ((Integer) obj).equals((Integer.valueOf(0)));
        Boolean isString = obj instanceof String && ((String) obj).equals((String.valueOf("")));

        if (null == obj || isDouble || isInteger || isString) {
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }
    
    
    public static void validateException (AncOfferRS ancOfferRS, int code, String message, String nativeMessage) {
        
        ancOfferRS.setGetAncillaryOffersRS(new GetAncillaryOffersRS());
        ancOfferRS.getGetAncillaryOffersRS().setServiceStatus(new ServiceStatus());
        ancOfferRS.getGetAncillaryOffersRS().getServiceStatus().setCode(code);
        ancOfferRS.getGetAncillaryOffersRS().getServiceStatus().setMessage(message);
        ancOfferRS.getGetAncillaryOffersRS().getServiceStatus().setNativeMessage(nativeMessage);
        
    }
    
    /**
     * 
     * Validate amount zero
     * 
     * @param amountStr
     * @return
     */
    public static boolean validateAmountZero(String amountStr) {
        boolean isZero = false;
        double amount = Double.parseDouble(amountStr);
        if (Math.abs(ZERO - amount) < THRESHOLD) {
            isZero = true;
        }
        return isZero;
    }

}
