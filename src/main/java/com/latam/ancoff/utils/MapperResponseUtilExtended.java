package com.latam.ancoff.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.latam.ancoff.entities.BaseAmount;
import com.latam.ancoff.entities.Portion;
import com.latam.ancoff.entities.Price;
import com.latam.ancoff.entities.PriceAdditionalBag;
import com.latam.ancoff.entities.Segments;
import com.latam.ancoff.entities.Tax;
import com.latam.ancoff.entities.TaxAmount;
import com.latam.ancoff.entities.Taxes;
import com.latam.ancoff.entities.TotalAmount;
import com.latam.pax.ancillary.offer.GetAncillaryOffersRQ;
import com.latam.pax.ancillary.offer.Offers;
import com.latam.pax.ancillary.offer.PriceElement;
import com.latam.pax.ancillary.offer.Offers.Segment;


public final class MapperResponseUtilExtended{

    private MapperResponseUtilExtended() {
        throw new IllegalStateException("Utility class");
    }
    
    public static boolean isPortionInfoSegments(String typeCode, AppProperties property) {
        boolean isPortionInfo = false;
        if (null != property.getTypeCodeInfoPortion() 
                && property.getTypeCodeInfoPortion().contains(typeCode)) {
            isPortionInfo = true;
        }
        return isPortionInfo;
    }
    
    /**
     * 
     * Method mapping portion
     * 
     * @param offers
     * @param portion
     * @param priceAdditionalBag
     * @param offersList
     * @param getAncillaryOffersRQ
     */
    public static void mapperPortion(Offers offers,
            Portion portion,
            PriceAdditionalBag priceAdditionalBag,
            GetAncillaryOffersRQ getAncillaryOffersRQ,
            boolean isPortionInfo) {
        
        if( null != offers.getAncillaryFee() ) {
            portion.setPrice(new Price());
            priceAdditionalBag.setPrice(new Price());
            
            boolean isBaseAmount = null != offers.getAncillaryFee().getBase() 
                    && null != offers.getAncillaryFee().getBase().getAmount();
            
            boolean isEquivAmount = null != offers.getAncillaryFee().getBase() 
                    && null != offers.getAncillaryFee().getBase().getEquivAmount();
            
            if ( isEquivAmount ) {
                portion.getPrice().setBaseAmount(new BaseAmount());
                portion.getPrice().getBaseAmount().setIsoCurrencyCode(offers.getAncillaryFee().getBase().getEquivAmount().getCurrency());
                portion.getPrice().getBaseAmount().setText(String.valueOf(offers.getAncillaryFee().getBase().getEquivAmount().getValue()));
                priceAdditionalBag.getPrice().setBaseAmount(new BaseAmount());
                priceAdditionalBag.getPrice().getBaseAmount().setIsoCurrencyCode(offers.getAncillaryFee().getBase().getEquivAmount().getCurrency());
                priceAdditionalBag.getPrice().getBaseAmount().setText(String.valueOf(offers.getAncillaryFee().getBase().getEquivAmount().getValue()));
            } else if (isBaseAmount){
                portion.getPrice().setBaseAmount(new BaseAmount());
                portion.getPrice().getBaseAmount().setIsoCurrencyCode(offers.getAncillaryFee().getBase().getAmount().getCurrency());
                portion.getPrice().getBaseAmount().setText(String.valueOf(offers.getAncillaryFee().getBase().getAmount().getValue()));
                priceAdditionalBag.getPrice().setBaseAmount(new BaseAmount());
                priceAdditionalBag.getPrice().getBaseAmount().setIsoCurrencyCode(offers.getAncillaryFee().getBase().getAmount().getCurrency());
                priceAdditionalBag.getPrice().getBaseAmount().setText(String.valueOf(offers.getAncillaryFee().getBase().getAmount().getValue()));
            }
            
            assignTotalAmount(offers, 
                    portion,
                    priceAdditionalBag);

            mapperTaxes(portion, priceAdditionalBag, offers);
            if ( isPortionInfo ) {
                mapperSegments(portion, getAncillaryOffersRQ, offers);
            }
            
        }
    }
    
    /**
     * 
     * Method setting value total amount
     * 
     * @param offers
     * @param portion
     * @param priceAdditionalBag
     */
    private static void assignTotalAmount(Offers offers, 
            Portion portion,
            PriceAdditionalBag priceAdditionalBag) {
        boolean isTotalAmount = null != offers.getAncillaryFee().getTTLPrice() 
                && null != offers.getAncillaryFee().getTTLPrice().getEquivAmount();
        boolean isAmount = null != offers.getAncillaryFee().getTTLPrice() 
                && null != offers.getAncillaryFee().getTTLPrice().getAmount();
        if( isTotalAmount ) {
            portion.getPrice().setTotalAmount(new TotalAmount());
            portion.getPrice().getTotalAmount().setIsoCurrencyCode(offers.getAncillaryFee().getTTLPrice().getEquivAmount().getCurrency());
            portion.getPrice().getTotalAmount().setText(String.valueOf(offers.getAncillaryFee().getTTLPrice().getEquivAmount().getValue()));
            priceAdditionalBag.getPrice().setTotalAmount(new TotalAmount());
            priceAdditionalBag.getPrice().getTotalAmount().setIsoCurrencyCode(offers.getAncillaryFee().getTTLPrice().getEquivAmount().getCurrency());
            priceAdditionalBag.getPrice().getTotalAmount().setText(String.valueOf(offers.getAncillaryFee().getTTLPrice().getEquivAmount().getValue()));
        } else if( isAmount ) {
            portion.getPrice().setTotalAmount(new TotalAmount());
            portion.getPrice().getTotalAmount().setIsoCurrencyCode(offers.getAncillaryFee().getTTLPrice().getAmount().getCurrency());
            portion.getPrice().getTotalAmount().setText(String.valueOf(offers.getAncillaryFee().getTTLPrice().getAmount().getValue()));
            priceAdditionalBag.getPrice().setTotalAmount(new TotalAmount());
            priceAdditionalBag.getPrice().getTotalAmount().setIsoCurrencyCode(offers.getAncillaryFee().getTTLPrice().getAmount().getCurrency());
            priceAdditionalBag.getPrice().getTotalAmount().setText(String.valueOf(offers.getAncillaryFee().getTTLPrice().getAmount().getValue()));
        }
    }
    
    /**
     * 
     * Method mapping Tax
     * 
     * @param portion
     * @param priceAdditionalBag
     * @param offers
     */
    public static void mapperTaxes(Portion portion,
            PriceAdditionalBag priceAdditionalBag, 
            Offers offers) {
        
        if ( null != offers.getAncillaryFee().getTax() ) {
            portion.getPrice().setTaxes(new Taxes());
            portion.getPrice().getTaxes().setTax(new ArrayList<Tax>());
            priceAdditionalBag.getPrice().setTaxes(new Taxes());
            priceAdditionalBag.getPrice().getTaxes().setTax(new ArrayList<Tax>());
            for( PriceElement taxOffer : offers.getAncillaryFee().getTax() ) {
                Tax tax = new Tax();
                tax.setTaxCode(taxOffer.getCode());
                if ( null != taxOffer.getEquivAmount() ) {
                    TaxAmount taxAmount = new TaxAmount();
                    taxAmount.setIsoCurrencyCode(taxOffer.getEquivAmount().getCurrency());
                    taxAmount.setText(String.valueOf(taxOffer.getEquivAmount().getValue()));
                    tax.setTaxAmount(taxAmount);
                } else if( null != taxOffer.getAmount() ) {
                    TaxAmount taxAmount = new TaxAmount();
                    taxAmount.setIsoCurrencyCode(taxOffer.getAmount().getCurrency());
                    taxAmount.setText(String.valueOf(taxOffer.getAmount().getValue()));
                    tax.setTaxAmount(taxAmount);
                }
                portion.getPrice().getTaxes().getTax().add(tax);
                priceAdditionalBag.getPrice().getTaxes().getTax().add(tax);
            }
        }
    }
    
    /**
     * Method mapping Segment
     * 
     * @param portion
     * @param getAncillaryOffersRQ
     * @param offersList
     */
    public static void mapperSegments(Portion portion, 
            GetAncillaryOffersRQ getAncillaryOffersRQ,
            Offers offers) {
        
        portion.setSegments(new Segments());
        portion.getSegments().setSegment(new ArrayList<com.latam.ancoff.entities.Segment>());
        if ( !offers.getSegment().isEmpty() ) {
            for ( Segment segment : offers.getSegment() ) {
                  List<com.latam.pax.ancillary.offer.Segment> segmentsRQ = getAncillaryOffersRQ.getQueryByItinerary().getSegment().stream()
                  .filter(value -> value.getId().equalsIgnoreCase(segment.getSegmentId()))
                  .collect(Collectors.toList());
                    if ( !segmentsRQ.isEmpty() ) {
                    segmentsRQ.forEach( segmentRQ -> {
    
                            com.latam.ancoff.entities.Segment seg = new com.latam.ancoff.entities.Segment();
                            seg.setDepartureAirport(segmentRQ.getFlightDetail().getDepartureAirport());
                            seg.setArrivalAirport(segmentRQ.getFlightDetail().getArrivalAirport());
                            seg.setFlightNumber(segmentRQ.getFlightDetail().getFlightNumber());
                            seg.setOperatingAirlineCode(segmentRQ.getFlightDetail().getOperatingAirline());
                            portion.getSegments().getSegment().add(seg);
                            portion.setSegmentNumber(segment.getSegmentNumber());
                    });
                }
            }
        }
    }
}
