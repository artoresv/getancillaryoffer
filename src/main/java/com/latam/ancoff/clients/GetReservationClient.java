package com.latam.ancoff.clients;

import java.net.URL;

import javax.xml.ws.BindingProvider;
import javax.xml.ws.Holder;
import javax.xml.ws.WebServiceException;

import com.latam.ancoff.entities.AncOfferRS;
import com.latam.ancoff.exception.AncillaryOfferException;
import com.latam.ancoff.utils.AppProperties;
import com.latam.ancoff.utils.Constants;
import com.latam.ancoff.utils.GetReservationUtil;
import com.latam.ancoff.utils.PrintUtil;
import com.latam.ancoff.utils.ValidationUtils;
import com.latam.host.sabre.ws.WSSabreSessionManager;
import com.latam.pax.getreservation.From;
import com.latam.pax.getreservation.GetReservationPortType;
import com.latam.pax.getreservation.GetReservationRQ;
import com.latam.pax.getreservation.GetReservationRS;
import com.latam.pax.getreservation.GetReservationService;
import com.latam.pax.getreservation.MessageData;
import com.latam.pax.getreservation.MessageHeader;
import com.latam.pax.getreservation.ReturnOptionsPNRB;
import com.latam.pax.getreservation.Security;
import com.latam.pax.getreservation.Service;
import com.latam.pax.getreservation.To;

import lombok.extern.slf4j.Slf4j;

/**
 * getReservation client class implementation
 * 
 * @author Sergio Mondaca
 *
 */
@Slf4j
public class GetReservationClient {

    
    private static GetReservationService serviceGetReservation;
    private AppProperties property;

    public GetReservationClient(AppProperties property) {
        super();
        this.property = property;
    }

    public GetReservationRS execute(WSSabreSessionManager wsSession, String recordLocator, AncOfferRS ancOfferRS)
            throws AncillaryOfferException {

        GetReservationRS response = new GetReservationRS();

        if (null == wsSession.getBinarySecurityToken() || wsSession.getBinarySecurityToken().isEmpty()) {
            
            ValidationUtils.validateException (ancOfferRS, Constants.WS_ERROR_NOT_DEFINED_CODE, property.getErrorNotDefinedStr(), property.getErrorSecurityToken());
                        
        } else {
            long t1 = System.currentTimeMillis();

            Holder<Security> securityHolder = new Holder<>(GetReservationUtil.getReservationGetSecurity(wsSession));

            From from = new From();
            from.getPartyId().add(GetReservationUtil.getReservationGetPartyId(wsSession));

            To to = new To();
            to.getPartyId().add(GetReservationUtil.getReservationGetParty(wsSession));

            MessageData messageData = GetReservationUtil.getReservationGetMessageData();

            Holder<MessageHeader> messageHeaderHolder = getMessageHeader(wsSession, from, to, messageData);

            GetReservationRQ request = new GetReservationRQ();
            ReturnOptionsPNRB optionsPNRB = getRequest(recordLocator, request);
            request.setReturnOptions(optionsPNRB);
            
            URL url = WSSabreSessionManager.class
                    .getResource(property.getGetReservationWSDL());

            GetReservationPortType getReservationPortType = getInstanceService(url).getGetReservationPortType();
            ((BindingProvider) getReservationPortType).getRequestContext()
                    .put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, wsSession.getEndPoint());

            response = executeClient(securityHolder, messageHeaderHolder, request, getReservationPortType, ancOfferRS);

            long t2 = System.currentTimeMillis();
            
            log.error("GetReservationClient.execute time: " + (t2 - t1) + ", " + wsSession.getBinarySecurityToken());

        }
        return response;
    }

    private ReturnOptionsPNRB getRequest(String recordLocator, GetReservationRQ request) {
        request.setVersion(property.getGetReservationVersion());
        request.setLocator(recordLocator);
        request.setRequestType(property.getReservationStateful());
        ReturnOptionsPNRB optionsPNRB = new ReturnOptionsPNRB();
        optionsPNRB.setViewName(property.getReservationOpenRes());
        optionsPNRB.setResponseFormat(property.getReservationSTL());
        return optionsPNRB;
    }

    private GetReservationRS executeClient(Holder<Security> securityHolder, Holder<MessageHeader> messageHeaderHolder,
            GetReservationRQ request, GetReservationPortType getReservationPortType, AncOfferRS ancOfferRS) {
        try {
            GetReservationRS response;
            log.info(PrintUtil.jaxbToString(request));
            response = getReservationPortType.getReservationOperation(messageHeaderHolder, securityHolder, request);
            log.info(PrintUtil.jaxbToString(response));
            return response;
        } catch (WebServiceException e) {
            log.error(e.getMessage(), e);
            
            ValidationUtils.validateException (ancOfferRS, Constants.WS_ERROR_CONNECTION_CODE, property.getErrorConnectionStr(), e.getMessage());
            return null;
        }
    }

    /**
     * Gets the message header.
     *
     * @param wsSession   the ws session
     * @param from        the from
     * @param to          the to
     * @param service     the service
     * @param messageData the message data
     * @return the message header
     */
    public Holder<MessageHeader> getMessageHeader(WSSabreSessionManager wsSession, From from, To to,
            MessageData messageData) {

        MessageHeader messageHeader = new MessageHeader();
        messageHeader.setVersion(Constants.MESSAGE_HEADER_VERSION);
        messageHeader.setId("");
        messageHeader.setFrom(from);
        messageHeader.setTo(to);
        messageHeader.setCPAId(wsSession.getIPCC());
        messageHeader.setConversationId(wsSession.getConversationId());
        messageHeader.setService(getService());
        messageHeader.setAction(property.getGetReservationSabreAction());
        messageHeader.setMessageData(messageData);
        return new Holder<>(messageHeader);
    }

    /**
     * Gets the service.
     *
     * @return the service
     */

    public Service getService() {
        Service servicews = new Service();
        servicews.setValue(property.getGetReservationService());
        servicews.setType(property.getGetReservationSabreType());
        return servicews;
    }

    public GetReservationService getInstanceService(URL url) {
        synchronized (GetReservationService.class) {
            if (null == serviceGetReservation) {
                if (null != url) {
                    serviceGetReservation = new GetReservationService(url);
                } else {
                serviceGetReservation = new GetReservationService();
            }
        }
        }
        return serviceGetReservation;
    }

}
