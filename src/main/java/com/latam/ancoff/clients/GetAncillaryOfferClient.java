package com.latam.ancoff.clients;

import java.net.URL;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

import javax.xml.ws.BindingProvider;
import javax.xml.ws.Holder;
import javax.xml.ws.WebServiceException;

import com.latam.ancoff.entities.AncOfferRQ;
import com.latam.ancoff.entities.AncOfferRS;
import com.latam.ancoff.entities.GetReservationResponse;
import com.latam.ancoff.exception.AncillaryOfferException;
import com.latam.ancoff.utils.AppProperties;
import com.latam.ancoff.utils.Constants;
import com.latam.ancoff.utils.GetAncillaryOfferUtil;
import com.latam.ancoff.utils.MapperGetAncillaryOffer;
import com.latam.ancoff.utils.PrintUtil;
import com.latam.ancoff.utils.ValidationUtils;
import com.latam.host.sabre.ws.WSSabreSessionManager;
import com.latam.pax.ancillary.offer.From;
import com.latam.pax.ancillary.offer.GetAncillaryOffersFault;
import com.latam.pax.ancillary.offer.GetAncillaryOffersPortType;
import com.latam.pax.ancillary.offer.GetAncillaryOffersRQ;
import com.latam.pax.ancillary.offer.GetAncillaryOffersRS;
import com.latam.pax.ancillary.offer.GetAncillaryOffersService;
import com.latam.pax.ancillary.offer.MessageData;
import com.latam.pax.ancillary.offer.MessageHeader2;
import com.latam.pax.ancillary.offer.Security2;
import com.latam.pax.ancillary.offer.Service2;
import com.latam.pax.ancillary.offer.To;

import lombok.extern.slf4j.Slf4j;

/**
 * GetAncillaryOfferClient client class implementation
 * 
 * @author Sergio Mondaca
 *
 */
@Slf4j
public class GetAncillaryOfferClient {

    private static Locale locale = new Locale(Constants.LOCALE_MIN, Constants.LOCALE_US_MAY);
    private static GetAncillaryOffersService serviceGetAncillary;
    private AppProperties property;

    public GetAncillaryOfferClient(AppProperties property) {
        super();
        this.property = property;
    }

    public GetAncillaryOffersRS execute(WSSabreSessionManager wsSession, GetReservationResponse reservationRS,
            GetAncillaryOffersRQ request, AncOfferRQ ancOfferRQ, AncOfferRS ancOfferRS) throws AncillaryOfferException {

        GetAncillaryOffersRS response = new GetAncillaryOffersRS();

        if (null == wsSession.getBinarySecurityToken() || wsSession.getBinarySecurityToken().isEmpty()) {

            ValidationUtils.validateException(ancOfferRS, Constants.WS_ERROR_NOT_DEFINED_CODE,
                    property.getErrorNotDefinedStr(), property.getErrorSecurityToken());

        } else {
            long t1 = System.currentTimeMillis();

            Holder<Security2> securityHolder = new Holder<>(GetAncillaryOfferUtil.ancillaryOfferGetSecurity(wsSession));

            From from = new From();
            from.getPartyId().add(GetAncillaryOfferUtil.ancillaryOfferGetPartyId(wsSession));

            To to = new To();
            to.getPartyId().add(GetAncillaryOfferUtil.ancillaryOfferGetParty(wsSession));

            MessageData messageData = getMessageData();

            Holder<MessageHeader2> messageHeaderHolder = getMessageHeader(wsSession, from, to, messageData);

            MapperGetAncillaryOffer.getRequest(request, reservationRS, ancOfferRQ, property);

            URL url = WSSabreSessionManager.class
                    .getResource(property.getGetAncillaryOfferWSDL());

            GetAncillaryOffersPortType getAncillaryOffersPortType = getInstanceService(url)
                    .getGetAncillaryOffersPort();
            ((BindingProvider) getAncillaryOffersPortType).getRequestContext()
                    .put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, wsSession.getEndPoint());

            response = executeClient(securityHolder, messageHeaderHolder, request, getAncillaryOffersPortType,
                    ancOfferRS);

            long t2 = System.currentTimeMillis();
            log.error("GetAncillaryOfferClient.execute time: " + (t2 - t1) + ", " + wsSession.getBinarySecurityToken());

        }
        return response;
    }

    private GetAncillaryOffersRS executeClient(Holder<Security2> securityHolder,
            Holder<MessageHeader2> messageHeaderHolder, GetAncillaryOffersRQ request,
            GetAncillaryOffersPortType getAncillaryOffersPortType, AncOfferRS ancOfferRS) {
        try {
            GetAncillaryOffersRS response;
            log.info(PrintUtil.jaxbToString(request));
            response = getAncillaryOffersPortType.getAncillaryOffers(messageHeaderHolder, securityHolder, request);
            log.info(PrintUtil.jaxbToString(response));
            return response;

        } catch (WebServiceException | GetAncillaryOffersFault e) {
            log.error(e.getMessage(), e);

            ValidationUtils.validateException(ancOfferRS, Constants.WS_ERROR_CONNECTION_CODE,
                    property.getErrorConnectionStr(), e.getMessage());
            return null;
        }

    }

    /**
     * Gets the message data.
     *
     * @return the message data
     */
    public MessageData getMessageData() {
        DateTimeFormatter formatDate = DateTimeFormatter.ofPattern(Constants.CLIENT_DATE_FORMAT, locale);
        DateTimeFormatter formatDateMID = DateTimeFormatter.ofPattern(Constants.CLIENT_DATE_FORMAT_MID, locale);
        MessageData messageData = new MessageData();
        messageData.setRefToMessageId(LocalDateTime.now().format(formatDateMID));
        messageData.setTimestamp(LocalDateTime.now().format(formatDate));
        messageData.setMessageId(LocalDateTime.now().format(formatDateMID));
        return messageData;
    }

    /**
     * Gets the message header.
     *
     * @param wsSession   the ws session
     * @param from        the from
     * @param to          the to
     * @param service     the service
     * @param messageData the message data
     * @return the message header
     */
    public Holder<MessageHeader2> getMessageHeader(WSSabreSessionManager wsSession, From from, To to,
            MessageData messageData) {

        MessageHeader2 messageHeader = new MessageHeader2();
        messageHeader.setVersion(Constants.MESSAGE_HEADER_VERSION);
        messageHeader.setId("");
        messageHeader.setFrom(from);
        messageHeader.setTo(to);
        messageHeader.setCPAId(wsSession.getIPCC());
        messageHeader.setConversationId(wsSession.getConversationId());
        messageHeader.setService(getService());
        messageHeader.setAction(property.getGetAncillaryOfferSabreAction());
        messageHeader.setMessageData(messageData);
        return new Holder<>(messageHeader);
    }

    /**
     * Gets the service.
     *
     * @return the service
     */

    public Service2 getService() {
        Service2 servicews = new Service2();
        servicews.setValue(property.getGetAncillaryOfferService());
        servicews.setType(property.getGetAncillaryOfferSabreType());
        return servicews;
    }

    public GetAncillaryOffersService getInstanceService(URL url) {
        synchronized (GetAncillaryOffersService.class) {
            if (null == serviceGetAncillary) {
                if (null != url) {
                    serviceGetAncillary = new GetAncillaryOffersService(url);
                } else {
                serviceGetAncillary = new GetAncillaryOffersService();
            }
        }
        }
        return serviceGetAncillary;
    }

}
