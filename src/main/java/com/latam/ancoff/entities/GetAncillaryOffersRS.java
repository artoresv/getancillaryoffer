package com.latam.ancoff.entities;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class GetAncillaryOffersRS implements Serializable {

    private static final long serialVersionUID = -3003395138179825960L;

    @Getter @Setter
    private ServiceStatus serviceStatus;
    
    @Getter @Setter
    private AncillaryElements ancillaryElements;
}
