package com.latam.ancoff.entities;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Price implements Serializable {

    private static final long serialVersionUID = 3626822765868730654L;
    
    @Getter @Setter
    private BaseAmount baseAmount;
    
    @Getter @Setter
    private TotalAmount totalAmount;
    
    @Getter @Setter
    private Taxes taxes;

    
}
