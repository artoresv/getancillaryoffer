package com.latam.ancoff.entities;

import java.io.Serializable;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
public class AgentPos implements Serializable {

    private static final long serialVersionUID = -5350816650909329997L;
    
    @Getter @Setter
    private String pcc;
    
}
