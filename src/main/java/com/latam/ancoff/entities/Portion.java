package com.latam.ancoff.entities;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@JsonIgnoreProperties(value = { "segmentNumber" })
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Portion implements Serializable {

    private static final long serialVersionUID = 9140830418362794086L;
    
    @Getter @Setter
    private String departureAirport;
    
    @Getter @Setter
    private String arrivalAirport;
    
    @Getter @Setter
    private Price price;
    
    @Getter @Setter
    private Segments segments;
    
    @Getter @Setter
    private String segmentNumber;
    
}
