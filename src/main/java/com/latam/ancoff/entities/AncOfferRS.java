package com.latam.ancoff.entities;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@XmlRootElement
public class AncOfferRS implements Serializable {

    private static final long serialVersionUID = -7235836123538269864L;

    @Getter @Setter
    private GetAncillaryOffersRS getAncillaryOffersRS;
}
