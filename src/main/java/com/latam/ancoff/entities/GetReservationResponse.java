package com.latam.ancoff.entities;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import com.latam.pax.getreservation.OpenReservationElementType;
import com.latam.pax.getreservation.PassengerPNRB;
import com.latam.pax.getreservation.PricedItineraryPNRB;
import com.latam.pax.getreservation.SegmentTypePNRB.Segment;
import com.latam.pax.getreservation.TicketDetailsType;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@XmlRootElement
public class GetReservationResponse {

    @Getter @Setter
    private String pcc;
    
    @Getter @Setter
    private String isoCurrencyCode;
    
    @Getter @Setter
    private List<Segment> segment; 
    
    @Getter @Setter
    private List<PassengerPNRB> passengers;
    
    @Getter @Setter
    private List<TicketDetailsType> ticketDetails;
    
    @Getter @Setter
    private List<PricedItineraryPNRB> pricedItinerary;
    
    @Getter @Setter
    private List<OpenReservationElementType> openReservationElementTypes;
    
    
}
