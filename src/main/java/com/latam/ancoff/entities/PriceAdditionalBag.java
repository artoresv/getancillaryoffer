package com.latam.ancoff.entities;

import java.io.Serializable;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import com.fasterxml.jackson.annotation.JsonInclude;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PriceAdditionalBag implements Serializable {

    private static final long serialVersionUID = -2218711745548110300L;
    
    @Getter @Setter
    private Price price;
    
    @Getter @Setter
    private String ancillaryGroupCode;
    
    @Getter @Setter
    private String ancillaryCode;
    
    @Getter @Setter
    private String ancillarySubCode;
    
    @Getter @Setter
    private String ancillaryDescription;

    
}
