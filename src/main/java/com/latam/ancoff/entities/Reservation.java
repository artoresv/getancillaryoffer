package com.latam.ancoff.entities;

import java.io.Serializable;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
public class Reservation implements Serializable {

    private static final long serialVersionUID = 6737533393584076798L;
    
    @Getter @Setter
    private String recordLocator;
    
}
