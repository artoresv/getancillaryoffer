package com.latam.ancoff.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Portions implements Serializable {

    private static final long serialVersionUID = -4137794231069677764L;
    
    @Getter @Setter
    private List<Portion> portion;
    
    public Portions() {
        portion = new ArrayList<>();
    }

    
}
