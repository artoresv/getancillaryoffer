package com.latam.ancoff.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
public class Segments implements Serializable {

    private static final long serialVersionUID = -8049029582984468218L;
    
    @Getter @Setter
    private List<Segment> segment;
    
    public Segments() {
        segment = new ArrayList<>();
    }

    
}
