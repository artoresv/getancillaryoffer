package com.latam.ancoff.entities;

import java.io.Serializable;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
public class GetAncillaryOffersRQ implements Serializable {

    private static final long serialVersionUID = -7235836123538269864L;

    @Getter @Setter
    private String isoCurrencyCode;
    
    @Getter @Setter
    private Reservation reservation; 
    
    @Getter @Setter
    private AgentPos agentPos;
    
    @Getter @Setter
    private Filter filter;
    
    @Getter @Setter
    private String groupByAncillary;
    
}
