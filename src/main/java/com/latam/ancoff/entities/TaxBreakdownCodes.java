package com.latam.ancoff.entities;

import java.io.Serializable;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
public class TaxBreakdownCodes implements Serializable {

    private static final long serialVersionUID = -2876486275409412467L;
    
    @Getter @Setter
    private String taxBreakdownCode;
    
}
