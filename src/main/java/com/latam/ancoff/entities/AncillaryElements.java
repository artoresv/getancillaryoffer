package com.latam.ancoff.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
public class AncillaryElements implements Serializable {

    private static final long serialVersionUID = -2024879051963467458L;
    @Getter @Setter
    private List<AncillaryElement> ancillaryElement;
    
    public AncillaryElements() {
        ancillaryElement = new ArrayList<>();
    }

    
}
