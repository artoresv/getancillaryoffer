package com.latam.ancoff.entities;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Filter implements Serializable {
    
    private static final long serialVersionUID = -7519663831036695128L;
    
    @Getter @Setter
    private AncillaryElements ancillaryElements;
    
}
