package com.latam.ancoff.entities;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SummaryBagInformation implements Serializable {

    private static final long serialVersionUID = -8508233242920502027L;
    
    @Getter @Setter
    private String totalBags;
    
    @Getter @Setter
    private String bagsByAllowance;
    
    @Getter @Setter
    private String paidBags;
    
    @Getter @Setter
    private String bagsByffpAllowance;

    
}
