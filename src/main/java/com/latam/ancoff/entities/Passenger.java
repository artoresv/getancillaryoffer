package com.latam.ancoff.entities;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Passenger implements Serializable {

    private static final long serialVersionUID = -4818977261686560811L;
    
    @Getter @Setter
    private String paxNameNumber;
    
    @Getter @Setter
    private String firstName;
    
    @Getter @Setter
    private String lastName;
    
    @Getter @Setter
    private String typeCode;

    
}
