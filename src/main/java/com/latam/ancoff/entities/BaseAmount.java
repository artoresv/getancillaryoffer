package com.latam.ancoff.entities;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class BaseAmount implements Serializable {

    private static final long serialVersionUID = -2687988395147810117L;
    
    @Getter @Setter
    private String isoCurrencyCode;
    
    @Getter @Setter
    private String text;

    
}
