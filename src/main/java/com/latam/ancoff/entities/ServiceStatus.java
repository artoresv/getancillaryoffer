package com.latam.ancoff.entities;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ServiceStatus implements Serializable {
    
    private static final long serialVersionUID = 3599221895095787237L;
    
    @Getter @Setter
    private int code;
    
    @Getter @Setter
    private String message;
    
    @Getter @Setter
    private String nativeMessage;
    
}
