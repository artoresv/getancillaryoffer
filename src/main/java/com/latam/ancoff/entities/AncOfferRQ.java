package com.latam.ancoff.entities;

import java.io.Serializable;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
public class AncOfferRQ implements Serializable {

    private static final long serialVersionUID = -7235836123538269864L;

    @Getter @Setter
    private GetAncillaryOffersRQ getAncillaryOffersRQ;
}
