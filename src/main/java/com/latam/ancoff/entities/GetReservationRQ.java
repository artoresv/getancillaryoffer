package com.latam.ancoff.entities;

import java.io.Serializable;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
public class GetReservationRQ implements Serializable{

    private static final long serialVersionUID = -2941403228022148175L;
    
    @Getter @Setter
    private String recordLocator;
}
