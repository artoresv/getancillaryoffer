package com.latam.ancoff.entities;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@XmlRootElement
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AncillaryElement implements Serializable {

    private static final long serialVersionUID = -5350816650909329997L;
    @Getter @Setter
    private String typeCode;
    
    @Getter @Setter
    private String groupCode;
    
    @Getter @Setter
    private String code;
    
    @Getter @Setter
    private String subCode;
    
    @Getter @Setter
    private String description;
    
    @Getter @Setter
    private String referenceNumber;
    
    @Getter @Setter
    private int quantity;
    
    @Getter @Setter
    private String segmentIndicatorCode;
    
    @Getter @Setter
    private String statusCode;
    
    @Getter @Setter
    private Passenger passenger;
    
    @Getter @Setter
    private Portions portions;
    
    @Getter @Setter
    private PriceAdditionalBag priceAdditionalBag;
    
    @Getter @Setter
    private SummaryBagInformation summaryBagInformation;
    
    @Getter @Setter
    private String number;
    
    @Getter @Setter
    private String reasonCode;
    
    public AncillaryElement (AncillaryElement ancillaryElement) {
        super();
        this.typeCode = ancillaryElement.typeCode;
        this.groupCode = ancillaryElement.groupCode;
        this.code = ancillaryElement.code;
        this.subCode = ancillaryElement.subCode;
        this.description = ancillaryElement.description;
        this.referenceNumber = ancillaryElement.referenceNumber;
        this.quantity = ancillaryElement.quantity;
        this.segmentIndicatorCode = ancillaryElement.segmentIndicatorCode;
        this.statusCode = ancillaryElement.statusCode;
        this.number = ancillaryElement.number;
        this.reasonCode = ancillaryElement.reasonCode;
        this.passenger = ancillaryElement.passenger;
        this.portions = ancillaryElement.portions;
        this.priceAdditionalBag = ancillaryElement.priceAdditionalBag;
        this.summaryBagInformation = ancillaryElement.summaryBagInformation;
    }

    public AncillaryElement() {
        super();
    }
    
}
