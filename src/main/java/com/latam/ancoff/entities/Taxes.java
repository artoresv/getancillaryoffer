package com.latam.ancoff.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Taxes implements Serializable {

    private static final long serialVersionUID = 3124984275850988346L;
    
    @Getter @Setter
    private List<Tax> tax;
    
    public Taxes() {
        tax = new ArrayList<>();
    }

    
}

