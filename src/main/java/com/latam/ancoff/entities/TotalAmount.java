package com.latam.ancoff.entities;

import java.io.Serializable;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
public class TotalAmount implements Serializable {

    private static final long serialVersionUID = 1238561165553932614L;
    
    @Getter @Setter
    private String isoCurrencyCode;
    
    @Getter @Setter
    private String text;
    
    

    
}
