package com.latam.ancoff.entities;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Tax implements Serializable {

    private static final long serialVersionUID = 4445778167291553897L;
    
    @Getter @Setter
    private String taxCode;
    
    @Getter @Setter
    private TaxAmount taxAmount;
    
    @Getter @Setter
    private TaxBreakdownCodes taxBreakdownCodes;

    
}
