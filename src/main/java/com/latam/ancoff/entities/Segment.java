package com.latam.ancoff.entities;

import java.io.Serializable;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
public class Segment implements Serializable {

    private static final long serialVersionUID = -1235774603997156984L;
    
    @Getter @Setter
    private String departureAirport;
    
    @Getter @Setter
    private String arrivalAirport;
    
    @Getter @Setter
    private String flightNumber;
    
    @Getter @Setter
    private String operatingAirlineCode;
    
}
