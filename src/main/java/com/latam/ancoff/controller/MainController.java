package com.latam.ancoff.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import com.latam.ancoff.entities.AncOfferRQ;
import com.latam.ancoff.entities.AncOfferRS;
import com.latam.ancoff.entities.GetAncillaryOffersRS;
import com.latam.ancoff.entities.ServiceStatus;
import com.latam.ancoff.exception.AncillaryOfferException;
import com.latam.ancoff.services.AncillaryOfferService;
import com.latam.ancoff.utils.ValidationUtils;

import lombok.extern.slf4j.Slf4j;

@ComponentScan("com.latam")
@RestController
@Slf4j
public class MainController {
    @Autowired
    AncillaryOfferService ancillaryService;

    /**
     * G.Carcamo Validar como se llama a este servicio. Por defecto quedar
     * 
     * @param appName
     * @param optlanSSID
     * @param request
     * @return
     */
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, value = "/commercial/inventory/reservationsandticketing/reservation/v1/ancillaryoffers")
    public ResponseEntity<AncOfferRS> process(@RequestHeader(value = "LAN-ApplicationName") String appName,
            @RequestHeader(required = false, value = "LAN-SSID") String lanSSID, @RequestBody AncOfferRQ request) {

        /**
         * Logica asociada a servicios, aqui deberia ejecutarse la logica para obtencion
         * de response.
         */
        AncOfferRS bodyrs = new AncOfferRS();
        try {
            Boolean isValidAncillary = null == request.getGetAncillaryOffersRQ()
                    || ValidationUtils.validateStringNotNull(request.getGetAncillaryOffersRQ().getReservation())
                    || ValidationUtils.validateStringNotNull(
                            request.getGetAncillaryOffersRQ().getReservation().getRecordLocator());
            
            if (isValidAncillary
                    || ValidationUtils.validateStringNotNull(request.getGetAncillaryOffersRQ().getIsoCurrencyCode())
                    || ValidationUtils.validateStringNotNull(request.getGetAncillaryOffersRQ().getAgentPos())
                    || ValidationUtils.validateStringNotNull( request.getGetAncillaryOffersRQ().getAgentPos().getPcc()) ){

                bodyrs.setGetAncillaryOffersRS(new GetAncillaryOffersRS());
                bodyrs.getGetAncillaryOffersRS().setServiceStatus(new ServiceStatus());
                bodyrs.getGetAncillaryOffersRS().getServiceStatus().setCode(-1);
                return new ResponseEntity<>(bodyrs, HttpStatus.NOT_FOUND);
            }
            long t1 = System.currentTimeMillis();
            bodyrs = ancillaryService.process(appName, lanSSID, request);
            long t2 = System.currentTimeMillis();
            log.error("Service time: " + (t2 - t1) + " ms");
            
        } catch (AncillaryOfferException e) {
            log.error("AncillaryOfferException : "+e);
            bodyrs.setGetAncillaryOffersRS(new GetAncillaryOffersRS());
            bodyrs.getGetAncillaryOffersRS().setServiceStatus(new ServiceStatus());
            bodyrs.getGetAncillaryOffersRS().getServiceStatus().setCode(e.getCode());
            bodyrs.getGetAncillaryOffersRS().getServiceStatus().setMessage(e.getMessage());
            bodyrs.getGetAncillaryOffersRS().getServiceStatus().setNativeMessage(e.getNativeMessage());
        }

        return new ResponseEntity<>(bodyrs, HttpStatus.OK);

    }

}
