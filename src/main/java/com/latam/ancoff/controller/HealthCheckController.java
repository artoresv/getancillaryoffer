package com.latam.ancoff.controller;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.health.AbstractHealthIndicator;
import org.springframework.boot.actuate.health.Health.Builder;
import org.springframework.stereotype.Component;

import com.latam.ancoff.utils.AppProperties;
import com.latam.ancoff.utils.Constants;
import com.latam.ancoff.utils.HealthCheckUtil;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class HealthCheckController extends AbstractHealthIndicator{

   
    private AppProperties appProperties;
    
    private HealthCheckUtil healthCheckUtil = new HealthCheckUtil();

    /**
     * 
     * @param prop
     * @param builder
     * @author Atorres
     * @param healthCheckUtil 
     */
    private void checkUPSessionCreateService(Builder builder, HealthCheckUtil healthCheckUtil) {
        boolean isUp = false;
        
        try {
            isUp = healthCheckUtil.checkURL(appProperties.getSessionCreateEndpoint());
        } catch (IOException e) {
            isUp = false;
            log.warn("error when connecting with SessionCreate", e);
        }
        
        if (isUp) {
            builder.up().withDetail(Constants.HEALTH_SESSION_CREATE, Constants.HEALTH_SERVICES_OK_MESSAGE).build();
        } else {
            builder.down().withDetail(Constants.HEALTH_SESSION_CREATE, Constants.HEALTH_HTTP_ERROR_CODE);
        }
    }
    
    
    /**
     * 
     * @param prop
     * @param builder
     * @author Atorres
     * @param healthCheckUtil 
     */
    private void checkUPSessionCloseService(Builder builder, HealthCheckUtil healthCheckUtil) {
        boolean isUp = false;
        
        try {
            isUp = healthCheckUtil.checkURL(appProperties.getSessionCloseEndpoint());
        } catch (IOException e) {
            isUp = false;
            log.warn("error when connecting with SessionClose", e);
        }
        if (isUp) {
            builder.up().withDetail(Constants.HEALTH_SESSION_CLOSE, Constants.HEALTH_SERVICES_OK_MESSAGE).build();
        } else {
            builder.down().withDetail(Constants.HEALTH_SESSION_CLOSE, Constants.HEALTH_HTTP_ERROR_CODE);
        }
    }

    
    public void doHealthCheck(Builder builder){
        
        checkUPSessionCreateService(builder, healthCheckUtil);
        checkUPSessionCloseService(builder, healthCheckUtil);
    }

    @Autowired
    public void setAppProperties(AppProperties appProperties) {
        this.appProperties = appProperties;
    }
    
    
}
