package com.latam.ancoff.services;

import com.latam.ancoff.entities.AncOfferRQ;
import com.latam.ancoff.entities.AncOfferRS;
import com.latam.ancoff.exception.AncillaryOfferException;

public interface AncillaryOfferService {

    AncOfferRS process(String appName, String lanSSID, AncOfferRQ request) throws AncillaryOfferException;

}
